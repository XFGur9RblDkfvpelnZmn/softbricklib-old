﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace REB.WebPortal2
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> lsVelden = new List<string>();
            lsVelden.Add("schkod");
            lsVelden.Add("schoms");
            lsVelden.Add("strt");
            Tabel t = new Tabel("dagschema",lsVelden);
            Response.Write("<table>");
            int i = 0;
            foreach (Record ls in t.LrRecords)
            {
                if (i < 100)
                {
                    if (i != 0)
                    {
                        Response.Write("<tr>");
                        Response.Write("<td>");
                        Response.Write(ls.SKey);
                        Response.Write("</td>");

                    }
                    int a = 0;
                    foreach (string s in ls.LsWaarden)
                    {
                        if (i != 0)
                        {
                            Response.Write("<td><input type='text' name='"+t.LrRecords[0].LsWaarden[a]+ls.SKey+"' value='");
                            Response.Write(s);
                            Response.Write("' /></td>");
                        }
                        else
                        {
                            Response.Write("<td><input type='text' name='" + t.LrRecords[0].LsWaarden[a] + "' value='");
                            Response.Write(s);
                            Response.Write("' /></td>");
                        }
                        a++;
                    }
                    Response.Write("</tr>");
                }
                i++;
            }
            Response.Write("</table>");
        }
    }
}
