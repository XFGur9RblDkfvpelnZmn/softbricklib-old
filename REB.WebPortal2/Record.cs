﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REB.WebPortal2
{
    public class Record
    {
        string sKey;
        List<string> lsWaarden;

        public List<string> LsWaarden
        {
            get
            {
                return lsWaarden;
            }
        }
        public string SKey
        {
            get
            {
                return sKey;
            }
        }

        public Record(string sKey,List<string> lsWaarden)
        {
            this.sKey = sKey;
            this.lsWaarden = lsWaarden;
        }
        public static List<Record> lrPopulateRecords(Dictionary<string, List<string>> dls)
        {
            List<Record> lr = new List<Record>();
            foreach (KeyValuePair<string, List<string>> kvp in dls)
            {
                lr.Add(new Record(kvp.Key, kvp.Value));
            }
            return lr;
        }
    }
}