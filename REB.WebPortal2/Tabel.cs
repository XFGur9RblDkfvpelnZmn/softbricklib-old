﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REB.WebPortal2
{
    public class Tabel
    {
        string sTabelNaam;
        List<Record> lrRecords = new List<Record>();
        public List<Record> LrRecords
        {
            get
            {
                return lrRecords;
            }
        }
        public Tabel(string sTabelNaam)
        {
            this.sTabelNaam = sTabelNaam;
            lrRecords.Add(new Record("0",SB.Algemeen.Proces.lsSqlSelHeaders("* from " + sTabelNaam)));
            lrRecords.AddRange(Record.lrPopulateRecords(SB.Algemeen.Proces.DlsSqlSel("* from " + sTabelNaam)));
            
        }
        public Tabel(string sTabelNaam,List<string> lsVelden)
        {
            this.sTabelNaam = sTabelNaam;
            string sVelden = string.Empty;
            foreach (string sVeld in lsVelden)
            {
                sVelden += sVeld + ",";
            }
            lrRecords.Add(new Record("0", SB.Algemeen.Proces.lsSqlSelHeaders(sVelden+" from " + sTabelNaam)));
            lrRecords.AddRange(Record.lrPopulateRecords(SB.Algemeen.Proces.DlsSqlSel(sVelden+" from " + sTabelNaam)));

        }
    }
}