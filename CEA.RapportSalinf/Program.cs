﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SB.Algemeen;
using System.Text.RegularExpressions;

namespace CEA.RapportSalinf
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                
                string sStartDat = args[0];
                string sEindDat = args[1];
                string sStreep = args[2];
                string sSeq = args[3];//(all,badnum,afdnum,salnum,uitzend,naam)
                string sLijst = args[4];
                //string sOverzicht = "letot";
                string sTmpFile = args[6].Replace("aa", "ra");
                string sTimeFrame = "1";//(day by day, week by week, etc)
                string sVisSql = "0";
                 string sTmpFile2 = string.Empty;
                 try
                 {
                     sTmpFile2 = args[10].Split('=')[1]; ;
                 }
                 catch (Exception ex)
                 {
                 }

                if (File.Exists(sTmpFile))
                {
                    string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssff") + ".wrk";
                    File.Copy(sTmpFile.Replace("aa","ra"), Parameter.SSbRoot + "log\\tmp" + sTimestamp);
                    sTmpFile = Parameter.SSbRoot + "log\\tmp" + sTimestamp;
                    Proces.lfiWrkFiles.Add(new FileInfo(sTmpFile));
                }
                if (File.Exists(sTmpFile2))
                {
                    string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssff") + ".wrk";
                    File.Copy(sTmpFile2.Replace("aa", "ra"), Parameter.SSbRoot + "log\\tmp" + sTimestamp);
                    sTmpFile2 = Parameter.SSbRoot + "log\\tmp" + sTimestamp;
                    Proces.lfiWrkFiles.Add(new FileInfo(sTmpFile));
                }
                FileInfo fiPTO = Proces.FiKostenPlaatsOverzicht(sStartDat, sEindDat, sSeq, sLijst, sTmpFile, sTimeFrame, sVisSql);
                FileInfo fiPTOGridrap = Proces.FiCsvrap(fiPTO.FullName);
                string sInhoudPTO = Proces.SLeesFile(fiPTOGridrap.FullName);
                
                //string sInhoudPTO = Proces.SLeesFile(Parameter.SSbRoot + "log\\csvrap_201111021535452688351.wrk");


                FileInfo fi = Proces.FiExpDagschemaCena(sStartDat, sEindDat);
                //FileInfo fi = Proces.FiExpDagschemaCena("111001", "111101");
                string sInhoud = Proces.SLeesFile(fi.FullName);
                Dictionary<string, List<string>> dls = new Dictionary<string, List<string>>();
                foreach (string s in Regex.Split(sInhoud, Environment.NewLine))
                {
                    if (s.Split(';').Length > 3)
                    {
                        string[] asData = s.Split(';');
                        string sKey = asData[0].Replace(" ", "") + ";" + asData[1].Replace(" ", "");
                        List<string> lsData = new List<string>();
                        for (int i = 2; i < asData.Length; i++)
                        {
                            lsData.Add(asData[i]);
                        }
                        dls.Add(sKey, lsData);
                    }
                }
                string sdt= DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
                string sPath = Parameter.SSbRoot + "log\\rapport" + sdt + ".wrk";
                StreamWriter sw = new StreamWriter(sPath);
                foreach (string s in Regex.Split(sInhoudPTO, Environment.NewLine))
                {
                    if (s.Split(';').Length > 3 && s.Contains("SB nr.;") == false)
                    {
                        string[] asData = s.Split(';');
                        int a;
                        if (asData[2].Replace(" ","").Length>=8 && asData[2].Replace(" ", "").Substring(2, 1) == "-" && asData[2].Replace(" ", "").Substring(5, 1) == "-")
                        {
                            
                            a = 2;
                        }
                        else
                        {
                            
                            a = 3;
                        }
                        
                        string sKey = asData[0].Replace(" ", "") + ";20" + asData[a].Replace(" ", "").Substring(6, 2) + asData[a].Replace(" ", "").Substring(3, 2) + asData[a].Replace(" ", "").Substring(0, 2);
                        sw.Write(asData[0] + " ; " + asData[1] + " ; " + asData[a]+" ; ");
                        if (a == 3)
                        {
                            sw.Write(asData[2] + " ; ");
                        }
                        try
                        {

                            for (int i = 0; i < 2; i++)
                            {
                                sw.Write(dls[sKey][i]+";");
                            }
                            //foreach (string s2 in dls[sKey])
                            //{
                            //    sw.Write(s2 + " ; ");
                            //}
                        }
                        catch (Exception ex)
                        {
                        }
                        
                        for (int i = a+1; i < asData.Length; i++)
                        {
                            sw.Write(asData[i] + " ; ");
                        }
                        sw.Write(Environment.NewLine);
                    }
                }
                sw.Close();
                new Proces("tc_lp log\\rapport" + sdt + ".wrk");

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                Proces.VerwijderWrkFiles();
            }
        }
        
    }
}
