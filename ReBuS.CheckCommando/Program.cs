﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
using System.Text.RegularExpressions;
namespace ReBuS.CheckCommando
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo fi = Proces.FiSqlsel("cmd1, cmd2, cmd3 from mnucmd");
            string sInhoud = Proces.SLeesFile(fi.FullName);
            foreach (string sLijn in Regex.Split(sInhoud, Environment.NewLine))
            {
                foreach (string sCmd in sLijn.Split(' '))
                {
                    if (sCmd.IndexOf(".bat") > -1 || sCmd.IndexOf("$") > 0)
                    {
                        if (!File.Exists(sCmd) && !File.Exists(sCmd.Replace('$',' ').Trim()))
                        {
                            Console.WriteLine(sCmd);
                        }
                    }
                }
            }
        }
    }
}
