﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace SB.Algemeen
{
    public class Proces
    {
        public static List<FileInfo> lfiWrkFiles = new List<FileInfo>();
        public Proces(string arg)
        {
            try
            {
                TextWriter tw = new StreamWriter(Parameter.SSbRoot + "log\\tmp.bat");
                
                if(Parameter.SMandant.Length>2)
                    tw.WriteLine("SET TC_MANDANT="+Parameter.SMandant);
                
                tw.WriteLine(arg);

                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Parameter.SSbRoot + "log\\tmp.bat");
               
                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;
                proces.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proces.StartInfo.CreateNoWindow = true;

                //start bat file
                proces.Start();
                proces.WaitForExit();
                Log.Verwerking(arg);
            }
            catch (Exception ex)
            {
                Log.LogException(string.Empty, ex);
            }
        }
        public Proces(List<string> args)
        {
            try
            {
                TextWriter tw = new StreamWriter(Parameter.SSbRoot + "log\\tmp.bat");
                tw.WriteLine("SET TC_MANDANT=" + Parameter.SMandant);
                foreach (string arg in args)
                {
                    tw.WriteLine(arg);
                }
                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Parameter.SSbRoot + "log\\tmp.bat");
                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;
                proces.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proces.StartInfo.CreateNoWindow = true;
                //start bat file
                proces.Start();
                proces.WaitForExit();
                Log.Verwerking(args);
            }
            catch (Exception ex)
            {
                Log.LogException(string.Empty, ex);
            }
        }

        public static void Gridrap(string sFile)
        {
            new Proces(Parameter.SSbRoot + "bin\\gridrap.exe scr/gridrap.scr \"" + sFile + "\" -r ");
        }
        public static void Tc_lp(string sFile)
        {
            new Proces(Parameter.SSbRoot + "bin\\tc_lp.exe \"" + sFile + "\" -r ");
        }
        public static void EventLog(string sCode,string sText)
        {
            new Proces(Parameter.SSbRoot + "bin\\clnt_exp event_log "+sCode+" \""+sText+"\"");
        }
        public static void PeriodeOverzicht(string sVanDat, string sTotDat, string sLijstNummer, string sInputFile )
        {
            new Proces(Parameter.SSbRoot + "bin\\pdeov2 " + sVanDat + " " + sTotDat + " - 05 " + sLijstNummer + " < \"" + sInputFile + "\"");
        }
        /// <summary>
        /// sSelectie 0-5 (all,badnum,afdnum,salnum,uitzend,naam)
        /// sTimeFrame 0-4 (day by day, week by week, etc)
        /// returnt de naam van het rapport
        /// </summary>
        /// <param name="sVanDat"></param>
        /// <param name="sTotDat"></param>
        /// <param name="sSelectie"></param>
        public static FileInfo FiPeriodeTotaalOverzicht(string sVanDat, string sTotDat, string sSelectie, string sLijstNummer, string sInputFile,string sTimeFrame,string sVisualSql)
        {
            string sFileName = Parameter.SSbRoot + "log\\pdetot2_" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + ".wrk";
            new Proces(Parameter.SSbRoot + "bin\\jrovz5pr " + sVanDat + " " + sTotDat + " - " + sSelectie + " " + sLijstNummer + " pdetot2 " + sInputFile + " 1 "+sTimeFrame+" "+sVisualSql+" grd=" + sFileName);
            FileInfo fi = new FileInfo(sFileName);
            lfiWrkFiles.Add(fi);
            return fi;
        }
        public static FileInfo FiCsvrap(string sFile)
        {
            string sFileName = Parameter.SSbRoot + "log\\csvrap_" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + ".wrk";
            new Proces(Parameter.SSbRoot + "bin\\csvrap.exe -i"+sFile+" -o"+sFileName+" -thh00 -r -h");
            FileInfo fi = new FileInfo(sFileName);
            lfiWrkFiles.Add(fi);
            return fi;
        }
        public static FileInfo FiSqlsel(string sSqlCommando)
        {
            string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            string sFileName = Parameter.SSbRoot + "log\\sqlsel" + sTimestamp + ".wrk";
            new Proces(Parameter.SSbRoot + "bin\\sqlsel \"" + sSqlCommando + "\" > " + sFileName +" -s");
            FileInfo fi = new FileInfo(sFileName);
            lfiWrkFiles.Add(fi);
            return fi;
        }
        public static FileInfo FiSqlsel(string sSqlCommando,List<string> args)
        {
            string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
            string sFileName = Parameter.SSbRoot + "log\\sqlsel" + sTimestamp + ".wrk";
            string sCmd = Parameter.SSbRoot + "bin\\sqlsel \"" + sSqlCommando + "\" > " + sFileName + " -s";
            foreach (string arg in args)
            {
                sCmd += " " + arg;
            }
            new Proces(sCmd);
            FileInfo fi = new FileInfo(sFileName);
            lfiWrkFiles.Add(fi);
            return fi;
        }
        public static string SLeesFile(string sFile)
        {
            StreamReader sr = new StreamReader(sFile);
            string sInhoud = sr.ReadToEnd();
            sr.Close();

            return sInhoud;
        }
        

        public static FileInfo FiExpDagschemaCena(string sVanDat, string sTotDat)
        {
            string sFileName = "exp_ds_cena_" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + ".wrk";
            new Proces(Parameter.SSbRoot + "bin\\clnt_exp exp_dagschema_cena " + sVanDat + " " + sTotDat + " "+Parameter.SSbRoot+"\\log "+sFileName);
            FileInfo fi = new FileInfo(Parameter.SSbRoot+"log\\"+sFileName);
            lfiWrkFiles.Add(fi);
            return fi;
        }
        public static DateTime dtStringToDate(string sDatum)
        {
            int iJaar = 0;
            int iMaand = 0;
            int iDag = 0;
            if (sDatum.Length == 6)
            {
                iJaar = 2000 + Convert.ToInt32(sDatum.Substring(0, 2));
                iMaand = Convert.ToInt32(sDatum.Substring(2, 2));
                iDag = Convert.ToInt32(sDatum.Substring(4, 2));
            }
            else if(sDatum.Length == 0)
            {
                iJaar = 2000;
                iMaand = 1;
                iDag = 1;
            }
            else
            {
                iJaar = Convert.ToInt32(sDatum.Substring(0, 4));
                iMaand = Convert.ToInt32(sDatum.Substring(4, 2));
                iDag = Convert.ToInt32(sDatum.Substring(6, 2));
            }

            DateTime dtDatum = new DateTime(iJaar, iMaand, iDag);
            return dtDatum;
        }
        public static void VerwijderWrkFiles()
        {

            foreach (FileInfo fi in lfiWrkFiles)
            {
                if (File.Exists(fi.FullName))
                    File.Delete(fi.FullName);
            }
            Log.Verwerking("wrk files verwijderd");
        }
    }
}
