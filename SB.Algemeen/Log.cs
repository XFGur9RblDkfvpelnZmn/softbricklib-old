﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SB.Algemeen
{
    public class Log
    {
        #region Fields

        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Log()
        {
        }

        #endregion

        #region Properties

        #endregion

        #region Methods

        public static void Verwerking(object oThis)
        {
            try
            {
                StreamWriter sw = File.AppendText(Parameter.SSbRoot + "log\\SBLib.log");
                sw.WriteLine();
                sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("---------------------");
                sw.WriteLine("Verwerking van: " + oThis.ToString());
                sw.WriteLine();
                sw.Close();
            }
            catch (Exception logEx)
            {
                LogException(oThis, logEx);
            }
        }
        public static void Verwerking(object oThis, string sBericht)
        {
            try
            {
                StreamWriter sw = File.AppendText(Parameter.SSbRoot + "log\\SBLib.log");
                sw.WriteLine();
                sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("---------------------");
                sw.WriteLine("Verwerking van: " + oThis.ToString());
                sw.WriteLine(sBericht);
                sw.WriteLine();
                sw.Close();
            }
            catch (Exception logEx)
            {
                LogException(oThis.ToString() + " - " + sBericht, logEx);
            }
        }
        public static void Verwerking(string sBericht)
        {
            try
            {
                StreamWriter sw = File.AppendText(Parameter.SSbRoot + "log\\SBLib.log");
                sw.WriteLine();
                sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("---------------------");
                sw.WriteLine(sBericht);
                sw.WriteLine();
                sw.Close();
            }
            catch (Exception logEx)
            {
                LogException(sBericht, logEx);
            }
        }
        public static void Exception(Exception ex)
        {
            try
            {
                Proces.EventLog("SBLIB","SB Library Error : Kijk SBLib.log na in de log folder");
                StreamWriter sw = File.AppendText(Parameter.SSbRoot + "log\\SBLib.log");
                sw.WriteLine();
                sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("---------------------");
                sw.WriteLine(ex.ToString());
                sw.WriteLine();
                sw.Close();
            }
            catch (Exception logEx)
            {
                LogException(ex, logEx);
            }
        }
        public static void LogException(object ex, Exception logEx)
        {
            
            StreamWriter sw = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "\\SBLib.log");
            sw.WriteLine();
            sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
            sw.WriteLine("---------------------");
            sw.WriteLine("LOG:");
            sw.WriteLine(ex.ToString());
            sw.WriteLine();
            sw.WriteLine("LOG EXCEPTION:");
            sw.WriteLine(logEx.ToString());
            sw.WriteLine();
            sw.Close();
        }
        
        #endregion

    }
}
