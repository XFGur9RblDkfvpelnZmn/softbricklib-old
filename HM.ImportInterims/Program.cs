﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
using System.Text.RegularExpressions;
namespace HM.ImportInterims
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string sInhoud = Proces.SLeesFile(args[0]);
                StreamWriter sw = new StreamWriter(args[1]);
                StreamWriter sw2 = new StreamWriter(args[2]);
                foreach (string sLijn in Regex.Split(sInhoud, Environment.NewLine))
                {
                    string sLijnSchrijf = string.Empty;
                    try
                    {
                        FileInfo fiCheckSalnumDatum = Proces.FiSqlsel("edatp from personeel where salnum = '" + sLijn.Split(';')[0] + "' and uitzend > '0'");
                        DateTime dtDatum = new DateTime(Convert.ToInt32(sLijn.Split(';')[4].Trim().Split('/')[2]), Convert.ToInt32(sLijn.Split(';')[4].Trim().Split('/')[0]), Convert.ToInt32(sLijn.Split(';')[4].Trim().Split('/')[1]));
                        dtDatum = dtDatum - new TimeSpan(1, 0, 0, 0);
                        
                        if (Proces.SLeesFile(fiCheckSalnumDatum.FullName).Trim() == dtDatum.ToString("yyyyMMdd"))
                        {
                            FileInfo fiCheckSalnumBeginDatum = Proces.FiSqlsel("sdatp from personeel where salnum = '" + sLijn.Split(';')[0] + "' and uitzend > '0'");
                            string sBeginDatum = Proces.SLeesFile(fiCheckSalnumBeginDatum.FullName).Trim();
                            DateTime dtBeginDatum = Proces.dtStringToDate(sBeginDatum);
                            sBeginDatum = dtBeginDatum.ToString("dd/MM/yyyy");
                            sLijnSchrijf = sLijn.Split(';')[0] + ";" + sLijn.Split(';')[1] + ";" + sLijn.Split(';')[2] + ";" + sLijn.Split(';')[3] + ";" + sBeginDatum +";"+ sLijn.Split(';')[5] + ";" + sLijn.Split(';')[6] + ";" + sLijn.Split(';')[7] + ";" + sLijn.Split(';')[8];
                        }
                        else
                        {
                            sLijnSchrijf = sLijn.Split(';')[0] + ";" + sLijn.Split(';')[1] + ";" + sLijn.Split(';')[2] + ";" + sLijn.Split(';')[3] + ";" + sLijn.Split(';')[4] + ";" + sLijn.Split(';')[5] + ";" + sLijn.Split(';')[6] + ";" + sLijn.Split(';')[7] + ";" + sLijn.Split(';')[8];
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Verwerking(sLijn, "Kon niet worden verwerkt");
                    }
                    FileInfo fiCheckSalnum = Proces.FiSqlsel("salnum from personeel where salnum = '" + sLijn.Split(';')[0] + "' and uitzend < '1'");
                    if (Proces.SLeesFile(fiCheckSalnum.FullName).Trim().Length > 1)
                    {
                        if(sLijnSchrijf.Trim().Length>0)
                            sw.WriteLine(sLijnSchrijf);
                    }
                    else
                    {
                        if (sLijnSchrijf.Trim().Length > 0)
                            sw2.WriteLine(sLijnSchrijf);
                    }
                }
                sw.Close();
                sw2.Close();
            }
            catch (IOException ex)
            {
                Log.Verwerking(ex, "Input/Output fout");
            }
            finally
            {
                Proces.VerwijderWrkFiles();
            }
        }
    }
}
