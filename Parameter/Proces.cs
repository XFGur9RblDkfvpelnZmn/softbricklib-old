﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Parameter
{
    public class Proces
    {
        public Proces(string arg)
        {
            try
            {
                TextWriter tw = new StreamWriter(Parameter.SSbRoot + "log\\tmp.bat");


                tw.WriteLine(arg);

                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Parameter.SSbRoot + "log\\tmp.bat");
                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;

                //start bat file
                proces.Start();
                proces.WaitForExit();
            }
            catch (Exception ex)
            {
                Log.LogException(string.Empty, ex);
            }
        }
        public Proces(List<string> args)
        {
            try
            {
                TextWriter tw = new StreamWriter(Parameter.SSbRoot + "log\\tmp.bat");

                foreach (string arg in args)
                {
                    tw.WriteLine(arg);
                }
                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Parameter.SSbRoot + "log\\tmp.bat");
                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;

                //start bat file
                proces.Start();
                proces.WaitForExit();
            }
            catch (Exception ex)
            {
                Log.LogException(string.Empty, ex);
            }
        }
    }
}
