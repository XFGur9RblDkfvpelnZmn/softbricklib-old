﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace Parameter
{
    public class Parameter
    {
        #region Fields

        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Parameter()
        {
        }

        #endregion

        #region Properties
        public static string SSbRoot
        {
            get
            {
                RegistryKey masterKey = Registry.LocalMachine.OpenSubKey("SOFTWARE").OpenSubKey("OrgaTime");
                return masterKey.GetValue("tcbase").ToString() + "\\";
            }
        }
        #endregion

        #region Methods

        #endregion

    }
}
