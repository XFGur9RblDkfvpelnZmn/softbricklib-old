﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SB.Algemeen;
using System.Text.RegularExpressions;

namespace CEA.WisFeestdag
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DateTime dtStartDatum = Proces.dtStringToDate(args[0]);
                DateTime dtEindDatum = Proces.dtStringToDate(args[1]);
                string sStartDatum = dtStartDatum.ToString("yyyyMMdd");
                string sEindDatum = dtEindDatum.ToString("yyyyMMdd");
                string sTmpFile = args[2];

                string sSeq = args[3];//(all,badnum,afdnum,salnum,uitzend,naam)

                Log.Verwerking("Verwerking wisfeest " + sTmpFile + " " + sStartDatum + "-" + sEindDatum);

                if (File.Exists(sTmpFile))
                {
                    DateTime dtDatum = dtStartDatum;

                    string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssff") + ".csv";
                    File.Copy(sTmpFile, Parameter.SSbRoot + "log\\tmp" + sTimestamp);
                    sTmpFile = Parameter.SSbRoot + "log\\tmp" + sTimestamp;
                    Proces.lfiWrkFiles.Add(new FileInfo(sTmpFile));
                    string sTmpInhoud = Proces.SLeesFile(sTmpFile);

                    while (dtDatum <= dtEindDatum)
                    {
                        
                      
                        string[] sCode = Regex.Split(sTmpInhoud.Trim(), Environment.NewLine);
                     
                        string sSalnumInhoud = sSqlCmd(sCode);
                      
                        //StreamWriter sw = new StreamWriter(Parameter.SSbRoot + "log\\wisfeestdag" + sTimestamp);
                        //if (Convert.ToInt32(sSeq) != 3)
                        //{
                        int i = 0;

                        string sCmd = "time, plan from proj_regi where van_date = '" + dtDatum.ToString("yyMMdd") + "' and ";
                        i = 0;
                        
                        StreamWriter sw = new StreamWriter(Parameter.SSbRoot + "log\\WisFeest.imp");
                        foreach (string sSalnum in Regex.Split(sSalnumInhoud, Environment.NewLine))
                        {
                            if (sSalnum.Length > 0)
                            {
                                
                                FileInfo fi = Proces.FiSqlsel("badnum from personeel where salnum = '" + sSalnum.Trim() + "'");
                                string sBadnum = Proces.SLeesFile(fi.FullName).Replace(Environment.NewLine, string.Empty).Trim();
                                
                                List<string> lsArgs = new List<string>();
                                lsArgs.Add("-d;");
                                lsArgs.Add("-i");
                                
                                fi = Proces.FiSqlsel("time, plan from proj_regi where van_date = '" + dtDatum.ToString("yyMMdd") + "' and badnum = '" + sBadnum + "'", lsArgs);
                                string sInhoud = Proces.SLeesFile(fi.FullName);
                               
                                string[] sInhoudLijn = Regex.Split(sInhoud, Environment.NewLine);
                                foreach (string sLijn in sInhoudLijn)
                                {
                                    if (sLijn.Split(';').Length > 2)
                                    {
                                        string[] sData = sLijn.Split(';');
                                        sw.WriteLine(sData[0] + ";" + sData[1] + ";" + sData[2] + ";0");
                                    }

                                }
                            }
                            //sw.WriteLine(sSalnum.Trim() + ";" + sDatum.Trim() + ";02;1000;1200;;;;");
                        }
                        sw.Close();

                        Proces p = new Proces("dbimport " + Parameter.SSbRoot + "log\\WisFeest.imp -d;");

                        //}
                        //else
                        //{
                        //    foreach (string sSalnum in Regex.Split(sTmpInhoud, Environment.NewLine))
                        //    {
                        //        sw.WriteLine(sSalnum.Trim() + ";" + sDatum.Trim() + ";;;;;;;");
                        //    }
                        //}
                        //sw.Close();
                        //File.Copy(Parameter.SSbRoot + "log\\wisfeestdag" + sTimestamp, Parameter.SSbRoot + "log\\wisfeestdag2" + sTimestamp);
                        //new Proces("clnt_exp imp_aanvraag_cena wisfeestdag" + sTimestamp + " 30");
                        //FileInfo fi = Proces.FiSqlsel("badnum from personeel");

                        //new Proces("salkrtmu - 1 2 "+sDatum+" "+sDatum+" 02 60000 < " + fi.FullName);
                        dtDatum = dtDatum.AddDays(1);
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                Proces.VerwijderWrkFiles();
            }

        }

        static string sSqlCmd(string[] sCode)
        {
            string sSalnums = string.Empty;
            for (int i = 0; i < sCode.Length;i++)
            {
                if (sCode[i].Trim().Length > 0)
                {
                    try
                    {
                        string sCmd = "salnum from personeel where badnum = '" + sCode[i].Substring(0, 5) + "' ";
                        try
                        {
                            FileInfo fiSalnums = Proces.FiSqlsel(sCmd);
                            string sSalnumInhoud = Proces.SLeesFile(fiSalnums.FullName);
                            sSalnums += sSalnumInhoud.Trim() + Environment.NewLine;
                        }
                        catch (FileNotFoundException fiex)
                        {
                            FileInfo fiSalnums = Proces.FiSqlsel(sCmd);
                            Log.Exception(fiex);
                            System.Threading.Thread.Sleep(1000);
                            string sSalnumInhoud = Proces.SLeesFile(fiSalnums.FullName);
                            sSalnums += sSalnumInhoud.Trim() + Environment.NewLine;
                        }

                        
                    }
                    catch (Exception ex)
                    {
                        Log.Exception(ex);
                    }
                }
            }
            
            return sSalnums;
        }
    }
}
