﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
using System.Net;

namespace REB.SlaControle
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string sKlant = System.Web.HttpUtility.UrlEncode(Parameter.SKlantNaam);
                Proces p = new Proces(Parameter.SSbRoot + "\\bin\\alles.bat");
                StreamReader sr = new StreamReader(Parameter.SSbRoot + "\\log\\SLA.log");
                string sSlaControle = sr.ReadToEnd().Replace(Environment.NewLine,"");
                sr.Close();
                string sSlaControleDef = string.Empty;
                sSlaControleDef += DateTime.Now.ToString("yyyyMMdd");
                foreach (string tmp in sSlaControle.Split(';'))
                {
                    sSlaControleDef += tmp.Trim()+";";

                }
                // Create a request using a URL that can receive a post. 
                WebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://rebus-portal.nl/sla/index.php");
                // Set the Method property of the request to POST.
                request.Method = "POST";
                // Create POST data and convert it to a byte array.
                string postData = "klant="+sKlant+"&postData=" + System.Web.HttpUtility.UrlEncode(sSlaControleDef);
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                //IWebProxy proxy = WebRequest.GetSystemWebProxy();
                //proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                //request.Proxy = proxy;
                // Set the ContentType property of the WebRequest.
                request.ContentType = "application/x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;
                request.PreAuthenticate = true;
                
                request.Timeout = 3 * 1000; // 3 seconds
                // Get the request stream.
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
                // Get the response.
                WebResponse response = request.GetResponse();
                // Display the status.
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                Log.Verwerking(reader.ReadToEnd()+"end");
                // Display the content.
                //Console.WriteLine(responseFromServer);
                // Clean up the streams.
                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }
    }
}
