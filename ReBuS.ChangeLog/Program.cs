﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
using System.Text.RegularExpressions;

namespace ReBuS.ChangeLog
{
    class Program
    {
        static DirectoryInfo diChangeLogDir = new DirectoryInfo(Parameter.SSbRoot + "log\\ChangeLog\\");
        static List<string> lsTabellen = new List<string>();
        static Dictionary<string, FileInfo> dSqlsels = new Dictionary<string, FileInfo>();
        static Dictionary<string, string> dInhoudSqlsels = new Dictionary<string, string>();
        static Dictionary<string, FileInfo> dLaatstBewaardeSqlsels = new Dictionary<string, FileInfo>();
        static Dictionary<string, string> dInhoudLaatstBewaardeSqlsels = new Dictionary<string, string>();
        static DateTime dtNu = DateTime.Now;
        

        /// <summary>
        /// Elk argument is een tabel waarvan de wijzigingen worden nagegaan/opgeslagen
        /// Voorbeeld: ChangeLog.exe syspar dagschema urencategorie
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //lees alle argumenten in en steek deze in lijst met tabellen
            lsTabellen.AddRange(args);

            //maak de changelog dir aan indien deze nog niet bestaat
            if (Directory.Exists(diChangeLogDir.FullName) == false)
                Directory.CreateDirectory(diChangeLogDir.FullName);

            //haal logfiles op
           
            FileInfo[] fiLogFiles = diChangeLogDir.GetFiles(System.Environment.GetEnvironmentVariable("TC_MANDANT") + "_*.log");
            

            #region lees data uit tabellen en steek de fileinfo's in lfiSqlsels

            //sqlsel parameters
            List<String> lsArgs = new List<string>();
            lsArgs.Add("-d;");
            lsArgs.Add("-i");

            //lees data uit tabellen
            foreach (string sTabel in lsTabellen)
            {
                FileInfo fi = Proces.FiSqlsel("* from " + sTabel, lsArgs);
                //indien de tabel nog niet is ingelezen
                if (!dSqlsels.ContainsKey(sTabel))
                {
                    dSqlsels.Add(sTabel, fi);
                    dInhoudSqlsels.Add(sTabel, Proces.SLeesFile(fi.FullName));
                }
            }

            #endregion

            #region zoek laatst bewaarde log/backup voor alle meegeleverde tabellen en lees inhoud

            //overloop alle logfiles
            foreach (FileInfo fi in fiLogFiles)
            {
                //overloop alle tabellen
                foreach(string sTabel in lsTabellen)
                {
                    //kijk na of het een logfile van dezelfde tabel betreft
                    //of er nog geen versie van bestaat
                    //of deze versie recenter is dan de laatst bewaarde
                    if ((fi.Name.IndexOf(sTabel) >= 0) && ((!dLaatstBewaardeSqlsels.ContainsKey(sTabel)) || (fi.LastWriteTime > dLaatstBewaardeSqlsels[sTabel].LastWriteTime)))
                    {
                        //verwijder mogelijke eerdere versies
                        dLaatstBewaardeSqlsels.Remove(sTabel);
                        //voeg huidige log toe
                        dLaatstBewaardeSqlsels.Add(sTabel, fi);
                    }
                }
            }

            //lees inhoud laatst bewaarde log
            foreach (KeyValuePair<string, FileInfo> kvpBewaardeSqlsel in dLaatstBewaardeSqlsels)
            {
                dInhoudLaatstBewaardeSqlsels.Add(kvpBewaardeSqlsel.Key, Proces.SLeesFile(kvpBewaardeSqlsel.Value.FullName));
            }

            #endregion

           //overloop alle tabellenn zoek verschillen en bewaar nieuwe versies
            foreach (string sTabel in lsTabellen)
            {
                if (dInhoudLaatstBewaardeSqlsels.ContainsKey(sTabel)&&dInhoudLaatstBewaardeSqlsels[sTabel]!=dInhoudSqlsels[sTabel])
                {
                    File.Copy(dSqlsels[sTabel].FullName, diChangeLogDir.FullName +System.Environment.GetEnvironmentVariable("TC_MANDANT")+"_"+ sTabel + dtNu.ToString("yyyyMMddHHmmss") + ".log");
                    LogVerschil(dInhoudLaatstBewaardeSqlsels[sTabel], dInhoudSqlsels[sTabel], sTabel);
                }
                else if (!dInhoudLaatstBewaardeSqlsels.ContainsKey(sTabel))
                {
                    File.Copy(dSqlsels[sTabel].FullName, diChangeLogDir.FullName + System.Environment.GetEnvironmentVariable("TC_MANDANT") + "_" + sTabel + dtNu.ToString("yyyyMMddHHmmss") + ".log");
                }
            }


            Proces.VerwijderWrkFiles();
        }

        static void LogVerschil(string sOud, string sNieuw,string sTabel)
        {
            string[] asRegelsOud = Regex.Split(sOud,Environment.NewLine);
            string[] asRegelsNieuw = Regex.Split(sNieuw, Environment.NewLine);
            
            FileInfo fiUsers = Proces.FiSqlsel("login from proto where date > '" + dLaatstBewaardeSqlsels[sTabel].LastWriteTime.ToString("yyyyMMdd") + "' or (date = '" + dLaatstBewaardeSqlsels[sTabel].LastWriteTime.ToString("yyyyMMdd") + "' and time >= '" + (Convert.ToInt32(dLaatstBewaardeSqlsels[sTabel].LastWriteTime.ToString("HH")) * 60 + Convert.ToInt32(dLaatstBewaardeSqlsels[sTabel].LastWriteTime.ToString("mm"))).ToString() +"'");
            string sInhoudUsers = Proces.SLeesFile(fiUsers.FullName);
            string[] asLogins = Regex.Split(sInhoudUsers, Environment.NewLine);
            List<string> lsLoginsGeenDubbel = new List<string>();
            StreamWriter sw = new StreamWriter(diChangeLogDir + System.Environment.GetEnvironmentVariable("TC_MANDANT") + "_Changes.log", true);
            sw.WriteLine("--------------- --------- ------------------------------ --------- ---------------");
            sw.WriteLine("--------------- --------- ------------------------------ --------- ---------------");
            sw.WriteLine("----Tabel: " + sTabel);
            sw.WriteLine("----Datum/Tijd: " + dtNu.ToString("yyyyMMddHHmmss"));
            sw.WriteLine("----Gebruikers:");
            foreach (string sLogin in asLogins)
            {
                if (!lsLoginsGeenDubbel.Contains(sLogin.Trim())&&sLogin.Trim().Length>0)
                {
                    lsLoginsGeenDubbel.Add(sLogin.Trim());
                    sw.WriteLine("--------" + sLogin.Trim());
                }
            }
            
            
            sw.WriteLine("--------------- --------- ------------------------------ --------- ---------------");
            sw.WriteLine("--------------- --------- ------------------------------ --------- ---------------");
            foreach (string sLijnNieuw in asRegelsNieuw)
            {
                string sVerschil = SZoekVerschil(sLijnNieuw, asRegelsOud);
                
                if (sVerschil != string.Empty)
                {
                    sw.WriteLine("--------------- --------- ---------------");
                    sw.WriteLine("Oud:");
                    sw.WriteLine(sVerschil);
                    sw.WriteLine("Nieuw:");                    
                    sw.WriteLine(sLijnNieuw);
                    sw.WriteLine("--------------- --------- ---------------");
                }

            }
            foreach (string sLijnOud in asRegelsOud)
            {
                string[] asDataOud = sLijnOud.Split(';');
                if (asDataOud.Length>=2&&(sNieuw.IndexOf(asDataOud[0] + ";" + asDataOud[1] + ";" + asDataOud[2] + ";") < 0))
                {
                    
                    sw.WriteLine("--------------- --------- ---------------");
                    sw.WriteLine("Oud:");
                    sw.WriteLine(sLijnOud);
                    sw.WriteLine("Nieuw:");
                    sw.WriteLine("*Verwijderd*");
                    sw.WriteLine("--------------- --------- ---------------");
                    
                }
            }
            foreach (string sLijnNieuw in asRegelsNieuw)
            {
                string[] asDataNieuw = sLijnNieuw.Split(';');
                if ((asDataNieuw.Length>=2)&&(sOud.IndexOf(asDataNieuw[0] + ";" + asDataNieuw[1] + ";" + asDataNieuw[2] + ";") < 0))
                {

                    sw.WriteLine("--------------- --------- ---------------");
                    sw.WriteLine("Oud:");
                    sw.WriteLine("*Onbestaande*");
                    sw.WriteLine("Nieuw:");
                    sw.WriteLine(sLijnNieuw);
                    sw.WriteLine("--------------- --------- ---------------");

                }
            }
            sw.Close();
        }
        static string SZoekVerschil(string sLijnNieuw, string[] asRegelsOud)
        {
            //voor asdata OUD = NIEUW, NIEUW=OUD
            //voor slijn niet

            string[] asDataNieuw = sLijnNieuw.Split(';');
            string sVerschil = string.Empty;
            foreach (string sLijnOud in asRegelsOud)
            {
                if (sLijnOud.Trim() != string.Empty)
                {
                   
                    string[] asDataOud = sLijnOud.Split(';');
                    if ((asDataNieuw[0].Trim() == asDataOud[0].Trim()) && (asDataNieuw[1].Trim() == asDataOud[1].Trim()) && (asDataNieuw[2].Trim() == asDataOud[2].Trim()))
                    {
                        if (asDataNieuw[3].Trim() != asDataOud[3].Trim())
                        {
                            sVerschil = sLijnOud;
                        }
                        
          
                    }


                }
            }
            return sVerschil;
        }
        

    }
}
