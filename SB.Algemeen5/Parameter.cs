﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.IO;

namespace SB.Algemeen
{
    public class Parameter
    {
        #region Fields

        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Parameter()
        {
        }

        #endregion

        #region Properties
        public static string SSbRoot
        {
            get
            {
                RegistryKey masterKey = Registry.LocalMachine.OpenSubKey("SOFTWARE").OpenSubKey("OrgaTime");
                if (masterKey != null)
                {
                    return masterKey.GetValue("tcbase").ToString() + "\\";
                }
                else
                {
                    return (Environment.CurrentDirectory+"\\..\\" );
                }
            }

        }
        public static string SMandant
        {
            get
            {
                return "DIS";
                //return "";
                //return Properties.Settings.Default.sMandant;
            }
        }
        public static string SWerkgeverNr
        {
            get
            {
                return "0000000";
            }
        }
        public static string SKlantNaam
        {
            get
            {
                List<string> lsArgs = new List<string>();
                lsArgs.Add("-s");
                FileInfo fiKlant = Proces.FiSqlsel("clientnam from syspar",lsArgs);
                return Proces.SLeesFile(fiKlant.FullName).Trim();
            }
        }
        #endregion

        #region Methods

        #endregion

    }
}
