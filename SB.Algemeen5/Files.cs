﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SB.Algemeen
{
    public class Files
    {
        #region Fields

        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Files()
        {
        }

        #endregion

        #region Properties

        #endregion

        #region Methods
        public static FileInfo fiGetLastUpdatedFileInDirectory(DirectoryInfo diDirectoryInfo, string sPattern)
        {
            FileInfo[] files = diDirectoryInfo.GetFiles(sPattern);

            FileInfo fiLastUpdatedFile = null;
            DateTime lastUpdate = new DateTime(2000, 1, 1);

            foreach (FileInfo file in files)
            {
                if (file.LastAccessTime > lastUpdate)
                {
                    fiLastUpdatedFile = file;
                    lastUpdate = file.LastAccessTime;
                }
            }

            return fiLastUpdatedFile;
        }
        #endregion

    }
}
