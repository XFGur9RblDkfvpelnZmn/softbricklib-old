﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
using System.Text.RegularExpressions;

namespace SB.PeriodeOverzicht
{
    public class PeriodeOverzicht
    {
        #region Fields

        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public PeriodeOverzicht(string sVanDat, string sTotDat, string sLijstNummer, string sInputFile)
        {
            //haal periode overzicht op
            Proces.PeriodeOverzicht(sVanDat, sTotDat, sLijstNummer, sInputFile);

            //zoek wrk bestand
            DirectoryInfo di = new DirectoryInfo(Parameter.SSbRoot + "log\\");
            FileInfo fi = Files.fiGetLastUpdatedFileInDirectory(di, "*.wrk");

            string sInhoud = Proces.SLeesFile(fi.FullName);
            string[] sDataBlokken = Regex.Split(sInhoud, "Periodeoverzicht ");

            foreach (string sDataBlok in sDataBlokken)
            {
                string[] sDataBlokLijn = Regex.Split(sDataBlok, Environment.NewLine);
                string sSalnum;
                if(sDataBlokLijn.Length>3)
                {
                    sSalnum = sDataBlokLijn[3].Substring(7, 10).Trim();
                    foreach (string sLijn in sDataBlokLijn)
                    {
                        string[] sData = sLijn.Split('|');
                        if ((sData.Length > 3)&&(sLijn.IndexOf("ds")<0))
                        {
                            string sDag = sData[0].Trim();
                            string sDagschema = sData[1].Trim();
                            string sInUit = sData[2].Trim();
                        }
                    }
                }
            }
        }

        #endregion

        #region Properties
        
        #endregion

        #region Methods
        
        #endregion

    }
}
