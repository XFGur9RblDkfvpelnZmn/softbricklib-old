﻿using System;
using System.Collections.Generic;
using System.Web;
using SB.Algemeen;
using System.IO;

namespace SB.WebPortal
{
    public class Gebruiker
    {
        
        /// <summary>
        /// kijkt na of login en pass overeen komen in SB (velden def in settings)
        /// return true wanneer akkoord
        /// gebruikt encryptie
        /// </summary>
        /// <param name="sLogin"></param>
        /// <param name="sPass"></param>
        /// <returns></returns>
        public static bool Login(string sLogin, string sPass)
        {            
            sLogin = sLogin.Trim();
            sPass = Encrypt(sPass.Trim());

            if (sLogin.Length > 0)
            {
                string sVelden = Properties.Settings.Default.gebruikerLoginVeld + ", " + Properties.Settings.Default.gebruikerPassVeld;
                Dictionary<string, List<string>> dlsLogin = Proces.DlsSqlSel(sVelden + " from personeel where " + Properties.Settings.Default.gebruikerLoginVeld + " = '" + sLogin + "'", DateTime.Now);
                if (dlsLogin.Count > 0 && dlsLogin[sLogin][0] == sPass)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// controleer oude pass en wijzig in nieuwe (encrypted)
        /// </summary>
        /// <param name="sLogin"></param>
        /// <param name="sPassNw"></param>
        /// <param name="sPassOud"></param>
        /// <returns></returns>
        public static bool ChangePass(string sLogin, string sPassNw, string sPassOud)
        {
            sLogin = sLogin.Trim();
            sPassNw = Encrypt(sPassNw.Trim());
            sPassOud = Encrypt(sPassOud.Trim());

            if (sLogin.Length > 0)
            {
                string sVelden = Properties.Settings.Default.gebruikerLoginVeld + ", " + Properties.Settings.Default.gebruikerPassVeld+", badnum";
                Dictionary<string, List<string>> dlsLogin = Proces.DlsSqlSel(sVelden + " from personeel where " + Properties.Settings.Default.gebruikerLoginVeld + " = '" + sLogin + "'", DateTime.Now);
                if (dlsLogin.Count > 0 && dlsLogin[sLogin][0] == sPassOud)
                {
                    string sPath = SB.Algemeen.Parameter.SSbRoot + "log\\dbimp" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + ".wrk";
                    StreamWriter sw = new StreamWriter(sPath);
                    sw.WriteLine("personeel;badnum=" + dlsLogin[sLogin][1] + ";" + Properties.Settings.Default.gebruikerPassVeld + ";" + sPassNw);
                    sw.Close();
                    new Proces(SB.Algemeen.Parameter.SSbRoot+"bin\\dbimport.exe "+sPath+" -e"+DateTime.Now.ToString("yyyyMMdd")+" -d;");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// returnt geencrypteerde string
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public static string Encrypt(string b)
        {
            decimal c = 0m;
            decimal d;
            int i;
            foreach (char a in b)
            {
                i = (int)(a);
                d = i / 38m;
                d = d + (45m / 100m);
                d = d * 4m;
                c += d; ;
            }
            c += 234.03m;
            return c.ToString("F6");
        }
    }
}