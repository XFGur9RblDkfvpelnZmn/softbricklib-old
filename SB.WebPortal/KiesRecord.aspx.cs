﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SB.Algemeen;

namespace SB.WebPortal
{
    public partial class KiesRecord : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                ddlTabel_SelectedIndexChanged(null, null);
        }
        
        protected void ddlTabel_SelectedIndexChanged(object sender, EventArgs e)
        {
            Dictionary<string, List<string>> dss = new Dictionary<string, List<string>>();
            switch (ddlTabel.SelectedItem.Value.Trim())
            {
                case "personeel":
                    dss = Proces.DlsSqlSel("badnum, naam from personeel");
                    laadtDictionary(dss, ddlSelectie.Items);
                    break;
                case "dagschema":
                    dss = Proces.DlsSqlSel("schkod, schoms from dagschema");
                    laadtDictionary(dss, ddlSelectie.Items);
                    break;
                case "syspar":
                    dss = Proces.DlsSqlSel("recnr, recnr from syspar");
                    laadtDictionary(dss, ddlSelectie.Items);
                    break;
                case "afdeling":
                    dss = Proces.DlsSqlSel("recnr, afdoms from afdeling");
                    laadtDictionary(dss, ddlSelectie.Items);
                    break;
            }
        }
        protected void laadtDictionary(Dictionary<string, List<string>> dss, ListItemCollection lic)
        {
            //if (!IsPostBack)
            {
                lic.Clear();
                foreach (KeyValuePair<string, List<string>> kvp in dss)
                {
                    lic.Add(new ListItem(kvp.Key + " " + kvp.Value[0], kvp.Key));
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("OpenRecord.aspx?tabel=" + ddlTabel.SelectedValue + "&key=" + ddlSelectie.SelectedValue);
        }
    }
}