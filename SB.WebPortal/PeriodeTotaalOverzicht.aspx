﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PeriodeTotaalOverzicht.aspx.cs" Inherits="SB.WebPortal.PeriodeTotaalOverzicht" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    Van:
    <asp:TextBox ID="txtStartDatum" runat="server" />
    Tot:
    <asp:TextBox ID="txtEindDatum" runat="server" />
    <br />
    <asp:DropDownList ID="ddlSelectie" runat="server" onload="ddlSelectie_Load" 
        onselectedindexchanged="ddlSelectie_SelectedIndexChanged" AutoPostBack="true" />
    <asp:DropDownList ID="ddlSelopp" runat="server" onload="ddlSelopp_Load" />
    <asp:ListBox ID="lbSelectie" runat="server" SelectionMode="Multiple" Rows="10" />
    <asp:TextBox ID="txtSel" runat="server" Visible="false" />
    <br />
    <asp:DropDownList ID="ddlOverzichten" runat="server" 
        onload="ddlOverzichten_Load" />
    </div>
    <div id="divResult" runat="server">
    </div>
    <asp:Button ID="btnSubmit" Text="Verzenden" runat="server" />
    </form>
</body>
</html>
