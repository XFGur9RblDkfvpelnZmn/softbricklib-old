﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SB.WebPortal
{
    public partial class _Default : System.Web.UI.Page
    {
        protected string sTabel;
        protected string sKey;
        DropDownList ddl = new DropDownList();
        Tabel t;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Gebruiker.Login("999999999 ", "ABCD"))
            {
                Session["sLogin"] = "999999999 ";
                Session["sPass"] = "ABCD";
                Session["badnum"] = SB.Algemeen.Proces.SLeesFile(SB.Algemeen.Proces.FiSqlsel("badnum from personeel where " + Properties.Settings.Default.gebruikerLoginVeld + " = '" + Session["sLogin"] + "'").FullName).Trim();

                //Dictionary<string, List<string>> dls = SB.Algemeen.Proces.DlsSqlSel("* from personeel where " + Properties.Settings.Default.gebruikerLoginVeld + " = '" + Session["sLogin"] + "'");
                //List<string> ls = SB.Algemeen.Proces.lsSqlSelHeaders("* from personeel");

                //Tabel t = new Tabel("personeel", Session["badnum"].ToString());
                //for (int i = 1; i < t.dlsWaarden[Session["badnum"].ToString()].Count; i++)
                //{
                //    Response.Write(t.lsVelden[i + 1] + " : " + t.dlsWaarden[Session["badnum"].ToString()][i] + "<br />");
                    
                //}
                
                //t.dlsWaarden[Session["badnum"].ToString()][8] = "test";
                //t.Opslaan();
            }
            //if (!IsPostBack)
            {
                laadtForm1(null, null);
            }
        }
        public void laadtForm1(object sender, System.EventArgs e)
        {

            
            //this.form1.Controls.Clear();
            sTabel = "personeel";
            if (Request["tabel"] != null)
            {
                sTabel = Request["tabel"].ToString();
            }
            if (Request["key"] != null)
            {
                sKey = Request["key"].ToString();
            }

            ddl.ID = "ddlKey";
            foreach (KeyValuePair<string, List<string>> kvp in SB.Algemeen.Proces.DlsSqlSel("* from " + sTabel))
            {
                ddl.Items.Add(new ListItem(kvp.Key, kvp.Key));

            }
            ddl.AutoPostBack = true;
            ddl.TextChanged += new System.EventHandler(this.laadtForm1);
            //form1.Controls.Add(ddl);
            Button b = new Button();
            b.Text = "Opslaan";
            b.ID = "Opslaan";

            b.Click += new EventHandler(opslaan);
            form1.Controls.Add(b);

            t = new Tabel(sTabel, ddl.SelectedValue);

            for (int i = 0; i < t.lsWaarden.Count; i++)
            {
                HtmlGenericControl div1 = new HtmlGenericControl("span");
                div1.ID = "div" + i;
                div1.InnerHtml = t.lsVelden[i + 1];
                try
                {
                    //this.form1.Controls.Add(div1);
                }
                catch (Exception ex)
                {
                }

                TextBox tb = new TextBox();
                tb.Text = t.lsWaarden[i];
                tb.ID = t.lsVelden[i + 1];
                //tb.AutoPostBack = true;

                try
                {
                    this.form1.Controls.Add(tb);
                }
                catch (Exception ex)
                {
                }

                HtmlGenericControl div2 = new HtmlGenericControl("span");
                div2.ID = "div2." + i;
                div2.InnerHtml = "<br />";
                try
                {
                    //this.form1.Controls.Add(div2);
                }
                catch (Exception ex)
                {
                }
                //test.InnerHtml += t.lsVelden[i + 1] + " : " + t.lsWaarden[i] + "<br />";
                //Response.Write(t.lsVelden[i + 1] + " : " + t.lsWaarden[i] + "<br />");

            }

        }
        protected void opslaan(object sender, EventArgs e)
        {
            int i=0;
            //t = new Tabel(sTabel, ddl.SelectedValue);
            foreach (Control tb in this.form1.Controls)
            {
                TextBox tbReal;
                try
                {
                    tbReal = (TextBox)tb;
                    t.lsWaarden[i] = tbReal.Text;

                    i++;
                }
                catch(Exception ex)
                {
                }
                
            }
            t.Opslaan();

        }
    }
}