﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KiesRecord.aspx.cs" Inherits="SB.WebPortal.KiesRecord" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList AutoPostBack="true" ID="ddlTabel" runat="server" OnSelectedIndexChanged="ddlTabel_SelectedIndexChanged">
                <asp:ListItem Text="Personeel" Value="personeel" />
                <asp:ListItem Text="Dagschema" Value="dagschema" />
                <asp:ListItem Text="Systeem Parameters" Value="syspar" />
                <asp:ListItem Text="Afdeling" Value="afdeling" />
            </asp:DropDownList>
            
            <asp:DropDownList ID="ddlSelectie"  runat="server" /><br />
            <asp:Button ID="btnSubmit" runat="server" Text="Verzenden" 
                onclick="btnSubmit_Click" style="height: 26px" />
        </div>
    </form>
</body>
</html>
