﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SB.WebPortal
{
    public partial class OpenRecord : System.Web.UI.Page
    {
        protected string sTabel;
        protected string sKey;
        Tabel t;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Gebruiker.Login("1100359  ", "ABCD"))
            {
                Session["sLogin"] = "1100359  ";
                Session["sPass"] = "ABCD";
                Session["badnum"] = SB.Algemeen.Proces.SLeesFile(SB.Algemeen.Proces.FiSqlsel("badnum from personeel where " + Properties.Settings.Default.gebruikerLoginVeld + " = '" + Session["sLogin"] + "'").FullName).Trim();

                sTabel = "personeel";
                
                if (Request["tabel"] != null)
                {
                    sTabel = Request["tabel"].ToString();
                    sKey = "1"; 
                }
                else
                {
                    sKey = Session["badnum"].ToString();
                }
                if (Request["key"] != null)
                {
                    sKey = Request["key"].ToString();
                }
                
                t = new Tabel(sTabel, sKey);

                VulForm();
            }
        }

        private void VulForm()
        {
            this.inhoud.InnerHtml = "";
            for (int i = 0; i < t.lsWaarden.Count; i++)
            {
                HtmlGenericControl div1 = new HtmlGenericControl("span");
                div1.ID = "div" + i;
                div1.InnerHtml = t.lsVelden[i + 1];
                try
                {
                    this.inhoud.Controls.Add(div1);
                }
                catch (Exception ex)
                {
                }

                TextBox tb = new TextBox();
                tb.Text = t.lsWaarden[i];
                tb.ID = t.lsVelden[i + 1];
                //tb.AutoPostBack = true;

                try
                {
                    this.inhoud.Controls.Add(tb);
                }
                catch (Exception ex)
                {
                }

                HtmlGenericControl div2 = new HtmlGenericControl("span");
                div2.ID = "div2." + i;
                div2.InnerHtml = "<br />";
                try
                {
                    this.inhoud.Controls.Add(div2);
                }
                catch (Exception ex)
                {
                }
                //test.InnerHtml += t.lsVelden[i + 1] + " : " + t.lsWaarden[i] + "<br />";
                //Response.Write(t.lsVelden[i + 1] + " : " + t.lsWaarden[i] + "<br />");

            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            int i = 0;
            //t = new Tabel(sTabel, ddl.SelectedValue);
            foreach (Control tb in this.inhoud.Controls)
            {
                TextBox tbReal;
                try
                {
                    tbReal = (TextBox)tb;
                    t.lsWaarden[i] = tbReal.Text;

                    i++;
                }
                catch (Exception ex)
                {
                }

            }
            t.Opslaan();
        }
    }
}