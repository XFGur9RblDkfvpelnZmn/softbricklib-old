﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
namespace SB.WebPortal
{
    public class Tabel
    {
        public List<string> lsVelden = new List<string>();
        public List<string> lsWaarden = new List<string>();
        string sTabelNaam;
        string sKey;
        
        public Tabel(string sTabelNaam,string sKey)
        {
            this.sTabelNaam = sTabelNaam;
            this.sKey = sKey;
            this.lsVelden = SB.Algemeen.Proces.lsSqlSelHeaders("* from "+sTabelNaam);
            this.lsWaarden = SB.Algemeen.Proces.DlsSqlSel("* from " + sTabelNaam + " where " + lsVelden[0].Replace("-","") + " = '" + sKey + "'")[sKey];
        }

        public bool Opslaan()
        {
            string sPath = SB.Algemeen.Parameter.SSbRoot + "log\\dbimp" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + ".wrk";
            StreamWriter sw = new StreamWriter(sPath);
            for(int i=0;i<lsWaarden.Count;i++)
            {
                sw.WriteLine(this.sTabelNaam + ";" + this.lsVelden[0] + "=" + sKey + ";" + lsVelden[i+1] + ";" + lsWaarden[i]);
            }
            sw.Close();
            SB.Algemeen.Proces p = new SB.Algemeen.Proces("dbimport "+sPath+" -e" + DateTime.Now.ToString("yyyyMMdd") + " -d;");
            return true;
        }
    }
    
}