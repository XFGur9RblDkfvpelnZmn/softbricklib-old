﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SB.Algemeen;
using System.Text.RegularExpressions;

namespace HM.VerwijderBadgenummers
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> lsArgs = new List<string>();
            lsArgs.Add("-d;");
            lsArgs.Add("-c"+DateTime.Now.ToString("yyyyMMdd"));
            lsArgs.Add("-i");
            FileInfo fiSelectBadnum = Proces.FiSqlsel("badnum2 from personeel where edatp < '" + DateTime.Now.ToString("yyyyMMdd") + "' and badnum2 <> 0",lsArgs);
            string sInhoud = Proces.SLeesFile(fiSelectBadnum.FullName);
            StreamWriter sw = new StreamWriter(Parameter.SSbRoot + "\\log\\verwijderBadnum.imp");
            foreach(string sLijn in Regex.Split(sInhoud,Environment.NewLine))
            {
                if (sLijn.Split(';').Length >= 3)
                {
                    string[] asData = sLijn.Split(';');
                    sw.WriteLine(asData[0] + ";" + asData[1] + ";" + asData[2]+";");
                }
            }
            sw.Close();
            new Proces(Parameter.SSbRoot + "\\bin\\dbimport.exe" + " " + Parameter.SSbRoot + "\\log\\verwijderBadnum.imp -d;");
            Proces.VerwijderWrkFiles();
            try
            {
                File.Delete(Parameter.SSbRoot + "\\log\\verwijderBadnum.imp");
            }
            catch (Exception ex)
            {
            }
        }
    }
}
