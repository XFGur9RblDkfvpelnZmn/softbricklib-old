﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
using System.Text.RegularExpressions;
namespace DEC.InterfaceAanvullendeLoongegevens
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string sStartDat = args[0];
                string sEindDat = args[1];
                string sStreep = args[2];
                string sSeq = args[3];//(all,badnum,afdnum,salnum,uitzend,naam)
                string sLijst = args[4];
                string sOverzicht = args[5];
                string sTmpFile = args[6];
                string sTimeFrame = "1";//(day by day, week by week, etc)
                string sVisSql = "0";
                if (File.Exists(sTmpFile))
                {
                    string sTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssff") + ".wrk";
                    File.Copy(sTmpFile, Parameter.SSbRoot + "log\\tmp" + sTimestamp);
                    sTmpFile = Parameter.SSbRoot + "log\\tmp" + sTimestamp;
                    Proces.lfiWrkFiles.Add(new FileInfo(sTmpFile));
                }
                FileInfo fiPTO = Proces.FiPeriodeTotaalOverzicht(sStartDat, sEindDat, sSeq, sLijst, sTmpFile, sTimeFrame, sVisSql);
                FileInfo fiPTOGridrap = Proces.FiCsvrap(fiPTO.FullName);
                string sInhoudPTO = Proces.SLeesFile(fiPTOGridrap.FullName);
                int i = 0;
                List<string> lsRegels = new List<string>();
                List<string> lsCodes = new List<string>();
                int iAantalCodes = 0;
                foreach (string sLijn in Regex.Split(sInhoudPTO, Environment.NewLine))
                {
                    try
                    {
                        string[] asData;
                    
                        asData = sLijn.Split(';');

                        if (i == 0)
                        {
                            iAantalCodes = asData.Length - 3;
                            for(i=0; i<iAantalCodes;i++)
                            {
                                lsCodes.Add(asData[3+i].Trim());
                            }
                        }
                        else if(sLijn.Trim().Length>3)
                        {
                            while (asData[0].Length < 7)
                            {
                                asData[0] = "0" + asData[0];
                            }
                            string[] sUren = new string[2];
                            for (i = 0; i < iAantalCodes; i++)
                            {
                                    if (asData[3 + i].Trim().Length != 0)
                                    {
                                        sUren = asData[3 + i].Trim().Split('.');
                                        while (sUren[0].Length < 7)
                                            sUren[0] = "0" + sUren[0];
                                        while (sUren[1].Length < 2)
                                            sUren[1] = sUren[0] + "0";
                                    }
                                    else
                                    {
                                        sUren = new string[2] { "0000000", "00" };
                                    }
                                    lsRegels.Add("3703900" + asData[0].Substring(0, 7) + asData[2].Trim().Split('-')[2] + asData[2].Trim().Split('-')[1] + asData[2].Trim().Split('-')[0] + lsCodes[i] + " " + sUren[0] + sUren[1]);
                                
                            }
                        
                        }
                        i++;
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        Log.Verwerking(ex, sLijn);
                    }
                }
                string sTimeStamp = DateTime.Now.ToString("yyyyMMddHHmmssff");
                StreamWriter sw = new StreamWriter(Parameter.SSbRoot + "log\\Loongeg"+sTimeStamp+".txt");
                foreach (string sRegel in lsRegels)
                {
                    sw.WriteLine(sRegel);
                }
                sw.Close();
                Proces.Tc_lp("log\\Loongeg" + sTimeStamp + ".txt");

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                Proces.VerwijderWrkFiles();
            }
        }
    }
}
