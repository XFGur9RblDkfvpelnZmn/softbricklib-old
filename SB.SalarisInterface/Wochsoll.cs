﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
using System.Globalization;
namespace SB.SalarisInterface
{
    public class Wochsoll
    {
        public string salnum;
        public int wochsoll, filler2, week;
        public static List<Wochsoll> lwWochsoll = new List<Wochsoll>();

        public Wochsoll(string salnum, int wochsoll, int filler2, int week)
        {
            this.salnum = salnum;
            this.wochsoll = wochsoll;
            this.filler2 = filler2;
            this.week = week;
        }
        static public int checkBijlageContract(int week, string salnum)
        {
            Wochsoll woch = zoekSalnumWeek(week, salnum);
            return (woch.wochsoll - woch.filler2) / 60 * 100;
        }
        static public Wochsoll zoekSalnumWeek(int week, string salnum)
        {
            Wochsoll ret = new Wochsoll("", 0, 0, 0);
            foreach (Wochsoll woch in lwWochsoll)
            {
                if ((woch.salnum == salnum) && (woch.week == week))
                {
                    ret = woch;
                }
            }
            return ret;
        }

        static public void MaakWochsollFiles(DateTime dtBegin,DateTime dtEind)
        {
            if (!Directory.Exists(Parameter.SSbRoot + @"log\Wochsoll"))
            {
                Directory.CreateDirectory(Parameter.SSbRoot + @"log\Wochsoll");
            }
            DateTime eind = dtEind;
            DateTime start = dtBegin;
            //BEREKEN WEEK VH JAAR
            CultureInfo myCI = new CultureInfo("nl-BE");
            Calendar myCal = myCI.Calendar;

            // Gets the DTFI properties required by GetWeekOfYear.
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
            int week = 0;
            int iWeek = myCal.GetWeekOfYear(dtBegin, myCWR, myFirstDOW);
            DayOfWeek laatsteDag = new DateTime(dtBegin.Year, 12, 31).DayOfWeek;
            if ((iWeek == 1) && (laatsteDag != DayOfWeek.Sunday))
            {
                week = myCal.GetWeekOfYear(new DateTime(dtBegin.Year, 12, 31), myCWR, myFirstDOW);
            }
            else
            {
                week = iWeek;
            }
            
            while (start.Year == eind.Year)
            {
                if (week > 53)
                {
                    week = 1;
                }
                new Proces("sqlsel -s -d; -c" + start.ToString("yyMMdd") + " \"personeel.filler1 personeel.wochsoll personeel.filler2 from personeel where personeel.wochsoll > personeel.filler2 and personeel.filler1 > 0\" > " + Parameter.SSbRoot + @"log\Wochsoll\week" + Convert.ToString(week) + ".wrk");
                week++;
                start = start.AddDays(Convert.ToDouble(7));
            }
        }

    }
}
