﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
using System.Globalization;
using System.Collections;

namespace SB.SalarisInterface
{
    public class Record : IComparable
    {
        #region Fields
        public string sSalnum, sWerkgeverNr, sDatum, sLooncode, sKp;
        public Int32 iUren;
        public static List<Record> lrRecords = new List<Record>();
        public int week;
        public DateTime dtDatum;
        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Record(string sSalnum,string sWerkgeverNr,string sDatum,string sLooncode, Int32 iUren,string sKp)
        {
            while (sSalnum.Length < 7)
                sSalnum = "0" + sSalnum;
            
            while (sSalnum.Length > 7)
                sSalnum = sSalnum.Substring(1);
            this.sSalnum = sSalnum;
            this.sWerkgeverNr = sWerkgeverNr;
            this.sDatum = sDatum;
            this.sLooncode = sLooncode;
            this.iUren = iUren;
            this.sKp = sKp;
            this.dtDatum = Proces.dtStringToDate(sDatum);
            //BEREKEN WEEK VH JAAR
            CultureInfo myCI = new CultureInfo("nl-BE");
            Calendar myCal = myCI.Calendar;

            // Gets the DTFI properties required by GetWeekOfYear.
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

            int week = myCal.GetWeekOfYear(dtDatum, myCWR, myFirstDOW);
            DayOfWeek laatsteDag = new DateTime(dtDatum.Year, 12, 31).DayOfWeek;
            if ((week == 1) && (laatsteDag != DayOfWeek.Sunday))
            {
                this.week = myCal.GetWeekOfYear(new DateTime(dtDatum.Year, 12, 31), myCWR, myFirstDOW);
            }
            else
            {
                this.week = week;
            }
        }

        #endregion

        #region Properties

        #endregion

        #region Methods
        public int CompareTo(object obj)
        {
            Record Compare = (Record)obj;
            string sortOn = this.sSalnum + this.sDatum;
            string sortOnObject = Compare.sSalnum + Compare.sDatum;
            int result = sortOn.CompareTo(sortOnObject);
            if (result == 0)
                result = sortOn.CompareTo(sortOnObject);
            return result;
        }
        public static Record RZoekRecord(string sSalnum, string sDatum, string sLooncode)
        {
            Record result = lrRecords.Find(
            delegate(Record r)
            {
                return ((r.sSalnum == sSalnum) && (r.sLooncode == sLooncode) && (r.sDatum == sDatum));
            }
            );
            if (result == null)
            {
                while (sSalnum[0] == '0')
                    sSalnum = sSalnum.Substring(1);
                result = lrRecords.Find(
                delegate(Record r)
                {
                    return ((r.sSalnum == sSalnum) && (r.sLooncode == sLooncode) && (r.sDatum == sDatum));
                }
                );
            }
            return result;
        }
        #endregion

        #region oude methods

        
        public void geenUren()
        {
            this.iUren = 0;
        }
        public void zet1013Om(List<Record> records, Werknemer persoon)
        {
            int aant1013 = this.iUren;
            foreach (Record record in records)
            {
                if ((record.sDatum == this.sDatum) && (record.sSalnum == this.sSalnum) && ((record.sLooncode == "1010") || (record.sLooncode == "1410")))
                {
                    int temp = (record.iUren - aant1013);
                    if (temp < 0)
                    {
                        aant1013 = temp * (-1);
                        temp = 0;
                    }
                    else
                    {
                        aant1013 = 0;
                    }
                    record.iUren = temp;

                    
                    if (record.iUren == 0)
                    {
                        record.sSalnum = "weg";
                    }
                }
            }
            //  WANNEER GEEN 1010/1410/1049 uren zijn wordt het 1013/1014 record verwijderd
            if (aant1013 == this.iUren)
            {
                this.sSalnum = "weg";
            }
        }
        public Record checkLooncode1049(Werknemer persoon)
        {
            Record newRec = new Record("", "", "", "", 0, "");
            if ((this.sLooncode == "1049") && (this.iUren > 800))
            {
                int aant1010 = this.iUren - 800;
                this.iUren = 800;
                
                newRec = new Record(this.sSalnum,Parameter.SWerkgeverNr, this.sDatum, "1010", aant1010, this.sKp);
            }
            return newRec;
        }
        public Record checkLooncode(Werknemer persoon, int aant1010)
        {
            Record newRec = new Record("", "", "", "", 0, "");

            if (aant1010 > 0)
            {

                //foreach (Record record in records)
                //{
                //    if ((record.Salnum == persoon.Salnum) && (record.LoonCode == "1010"))
                //    {
                //        aant1010 += Convert.ToInt32(record.Uren);
                //    }
                //}
                //aant1010 = Convert.ToInt32(persoon.Nominaal) - Convert.ToInt32(persoon.Contracturen);
                foreach (Record record in Record.lrRecords)
                {

                    if ((record.sSalnum == persoon.sSalnum) && (record.sLooncode == "1010") && (aant1010 > 0) && (record.week == this.week) && (record.dtDatum.DayOfWeek != DayOfWeek.Sunday))
                    {
                        if (record.iUren == aant1010)
                        {
                            record.sLooncode = "1046";
                            aant1010 = 0;
                        }
                        else if (record.iUren < aant1010)
                        {
                            record.sLooncode = "1046";
                            aant1010 = aant1010 - record.iUren;
                        }
                        else
                        {
                            record.iUren = record.iUren - aant1010;
                            

                            newRec = new Record(record.sSalnum,Parameter.SWerkgeverNr, record.sDatum, "1046", aant1010, record.sKp);

                            aant1010 = 0;
                        }
                    }
                }
                persoon.sWochsoll = persoon.sFiller2;
            }

            return newRec;
        }
        
        public void checkContract(Werknemer persoon, ArrayList records)
        {
            int totaal = 0;
            foreach (Record record in records)
            {

                if ((persoon.sSalnum == record.sSalnum) && (this.week == record.week) && (record.sLooncode != "9876") && (record.sSalnum != "weg"))
                {
                    totaal += record.iUren;
                }

            }
            if ((persoon.sMandant == "DIS") && (totaal > Convert.ToInt32(persoon.sFiller2) / 60 * 100) && (this.sLooncode == "1010"))
            {
                if ((totaal - Convert.ToInt32(persoon.sFiller2) / 60 * 100) >= this.iUren)
                {
                    this.iUren = 0;
                }
                else if ((totaal - Convert.ToInt32(persoon.sFiller2) / 60 * 100) < this.iUren)
                {
                    this.iUren = this.iUren - (totaal - Convert.ToInt32(persoon.sFiller2) / 60 * 100);
                }

            }
        }
        public void checkRoulement(Werknemer persoon)
        {
            int totaal = 0;
            //Record newRec = new Record("", "", "", "", "", "") ;
            if (persoon.sMa_grp == "1")
            {
                //DateTime[] data = new DateTime[7];
                Record[] week = new Record[7];
                totaal = 0;

                foreach (Record record in Record.lrRecords)
                {
                    if ((persoon.sSalnum == record.sSalnum) && (this.week == record.week) && (record.sLooncode != "9876") && (record.sSalnum != "weg"))
                    {
                        totaal += record.iUren;
                        switch (record.dtDatum.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                //data[0] = datum;
                                if (week[0] == null)
                                {
                                    week[0] = record;
                                }
                                else if (week[0].iUren == 0)
                                {
                                    week[0] = record;
                                }
                                break;
                            case DayOfWeek.Tuesday:
                                //data[1] = datum;
                                if (week[1] == null)
                                {
                                    week[1] = record;
                                }
                                else if (week[1].iUren == 0)
                                {
                                    week[1] = record;
                                }
                                break;
                            case DayOfWeek.Wednesday:
                                //data[2] = datum;
                                if (week[2] == null)
                                {
                                    week[2] = record;
                                }
                                else if (week[2].iUren == 0)
                                {
                                    week[2] = record;
                                }
                                break;
                            case DayOfWeek.Thursday:
                                //data[3] = datum;
                                if (week[3] == null)
                                {
                                    week[3] = record;
                                }
                                else if (week[3].iUren == 0)
                                {
                                    week[3] = record;
                                }
                                break;
                            case DayOfWeek.Friday:
                                //data[4] = datum;
                                if (week[4] == null)
                                {
                                    week[4] = record;
                                }
                                else if (week[4].iUren == 0)
                                {
                                    week[4] = record;
                                }
                                break;
                            case DayOfWeek.Saturday:
                                //data[5] = datum;
                                if (week[5] == null)
                                {
                                    week[5] = record;
                                }
                                else if (week[5].iUren == 0)
                                {
                                    week[5] = record;
                                }
                                break;
                            case DayOfWeek.Sunday:
                                //data[6] = datum;
                                if (week[6] == null)
                                {
                                    week[6] = record;
                                }
                                else if (week[6].iUren == 0)
                                {
                                    week[6] = record;
                                }
                                break;
                        }

                    }
                }
                int aantal = 0;
                int vrij = 0;
                int vol = 0;
                for (int i = 0; i < 7; i++)
                {
                    try
                    {
                        if (week[i].iUren > 0)
                        {
                            aantal++;
                            vol = i;
                        }
                        else if ((i != 5) && (i != 6))
                        {
                            vrij = i;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                //DateTime vrijeDag = data[vol];
                //vrijeDag = vrijeDag.AddDays(vrij - vol);
                if ((aantal == 4) && (totaal >= 3500))
                {
                    //newRec.werkgeverNr = werkgeverNr;
                    //newRec.salarisNr = salarisNr;
                    //newRec.loonCode = "9450";
                    //newRec.uren = "0000";
                    //newRec.kp = "       ";
                    //string jaar = Convert.ToString(vrijeDag.Year).Substring(2,2);
                    //string maand = Convert.ToString(vrijeDag.Month);
                    //if (vrijeDag.Month < 10)
                    //{
                    //    maand = "0" + maand;
                    //}
                    //string dag = Convert.ToString(vrijeDag.Day);
                    //if (vrijeDag.Day < 10)
                    //{
                    //    dag = "0" + dag;
                    //}
                    //newRec.datum = jaar + maand + dag;
                    week[vrij].sLooncode = "9450";

                }

            }

            //return newRec;
        }
        public void removeUren(Werknemer persoon)
        {
            int aant1010 = Convert.ToInt32(this.iUren);
            foreach (Record record in Record.lrRecords)
            {
                if ((record.sSalnum == persoon.sSalnum) && (record.sLooncode == "1010") && (aant1010 > 0) && (this.week == record.week))
                {
                    if (record.iUren - 300 == aant1010)
                    {
                        record.iUren = 300;
                        aant1010 = 0;
                    }
                    else if (record.iUren - 300 < aant1010)
                    {
                        if (record.iUren > 300)
                        {
                            aant1010 = aant1010 - (record.iUren - 300);
                            record.iUren = 300;
                        }
                    }
                    else
                    {
                        record.iUren = record.iUren - aant1010;

                        aant1010 = 0;
                    }
                }
            }
            if (this.bestaatRecordZelfdeDag())
            {
                this.sSalnum = "weg";
            }
            else
            {
                this.iUren = 0;
                this.sLooncode = "0000";
            }

        }
        public ArrayList omBoek1041(Werknemer persoon)
        {
            ArrayList omgeboekt = new ArrayList();

            int aant1010 = this.iUren;
            foreach (Record record in Record.lrRecords)
            {
                if ((record.sSalnum == persoon.sSalnum) && (record.sLooncode == "1010") && (aant1010 > 0) && (this.week == record.week) && (record.dtDatum.DayOfWeek != DayOfWeek.Sunday))
                {
                    if (record.iUren == aant1010)
                    {
                        record.iUren = 0;
                        record.sLooncode = "0000";
                        Record nieuw = new Record(record.sSalnum,Parameter.SWerkgeverNr, record.sDatum, this.sLooncode, aant1010, record.sKp);
                        omgeboekt.Add(nieuw);
                        aant1010 = 0;
                    }
                    else if (record.iUren < aant1010)
                    {

                        aant1010 = aant1010 - record.iUren;
                        int aant1041 = record.iUren;
                        record.iUren = 0;
                        record.sLooncode = "0000";
                        Record nieuw = new Record(record.sSalnum,Parameter.SWerkgeverNr, record.sDatum, this.sLooncode, aant1041, record.sKp);
                        omgeboekt.Add(nieuw);

                    }
                    else
                    {
                        record.iUren = record.iUren - aant1010;
                        
                        Record nieuw = new Record(record.sSalnum,Parameter.SWerkgeverNr, record.sDatum, this.sLooncode, aant1010, record.sKp);
                        omgeboekt.Add(nieuw);
                        aant1010 = 0;
                    }
                }

            }
            this.iUren = 0;
            this.sLooncode = "0000";
            return omgeboekt;
        }
        public void checkDubbel(List<Record> records)
        {
            foreach (Record record in records)
            {
                if ((this.sDatum == record.sDatum) && (this.sKp == record.sKp) && (this.sLooncode == record.sLooncode) && (this.sSalnum == record.sSalnum))
                {
                    string salnum = this.sSalnum;
                    this.sSalnum = "weg";
                    if (record.sSalnum == "weg")
                    {
                        this.sSalnum = salnum;
                    }
                    else
                    {
                        int uren = record.iUren;
                        int bijtel = this.iUren;
                        uren = uren + bijtel;
                        record.iUren = uren;
                    }
                }

            }
        }
        public Boolean bestaatRecordZelfdeDag()
        {
            foreach (Record record in Record.lrRecords)
            {
                string salnum = this.sSalnum;
                // CHECK OF THIS HETZELFDE RECORD IS ALS RECORD
                //zet salarisnummer van this op "weg"
                this.sSalnum = "weg";
                int checkZelfde = 1;
                //als het salarisnummer van record niet gelijk is aan "weg" gaat het om een ander record
                if (record.sSalnum != "weg")
                {
                    checkZelfde = 0;
                }
                //salnum terug op originele plaatsen
                this.sSalnum = salnum;
                if ((checkZelfde == 0) && (record.sSalnum == this.sSalnum) && (record.sDatum == this.sDatum))
                {
                    return true;
                }

            }
            return false;
        }
        #endregion
    }
}
