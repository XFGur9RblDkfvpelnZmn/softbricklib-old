﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
namespace SB.SalarisInterface
{
    public class Blox : SalarisFile
    {
        #region Fields
        public static Blox sf = null;
        public static List<string> lsSalnums = new List<string>();
        public static List<string> lsSalnumsTeLang = new List<string>();
        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Blox(string sStartDat, string sEindDat)
            : base("expblox1.txt", "expblox_Verwerkt.txt","expblox_Uitzondering.txt")
        {
            
        }

        #endregion

        #region Properties

        #endregion

        #region Methods
        public void writeFile()
        {
            Log.Verwerking(this,"Schrijf salarisfile "+sResultaatFileNaam+sTimeStamp);  
            StreamWriter file = new System.IO.StreamWriter(Parameter.SSbRoot + @"log\SalinfLog\\" + sResultaatFileNaam + sTimeStamp);
            //foreach (Record record in records)
            //{
            //    Personeel persoon = Personeel.zoekSalnum(werknemers, record.Salnum);
            //    record.WerkgeverNr = persoon.Team;
            //    if (record.Salnum != "weg")
            //    {
            //        record.Salnum = persoon.Filler1;
            //    }
            //}
            Record.lrRecords.Sort();
            foreach (string sSalnum in lsSalnumsTeLang)
            {
                if (lsSalnums.Contains(sSalnum))
                {
                    Blox.sf.schrijfUitzondering("Werknemer met te lang salarisnummer:" + sSalnum, string.Empty);
                }
            }
            foreach (Record record in Record.lrRecords)
            {
                //Werknemer persoon = Werknemer.WZoekWerknemer(record.sSalnum);
                if (record.sSalnum != "weg")
                {
                    Werknemer w = Werknemer.WZoekWerknemer(record.sSalnum);
                    if (w.sSalnum == "0999802")
                    {
                    }
                    if (w.sSalif == "1"&&w.sFiller1!=string.Empty&&lsSalnums.Contains(w.sSalnum)&&w.sTeamkod!=string.Empty)
                    {
                        string sFiller1 = w.sFiller1;
                        while (sFiller1.Length < 7)
                        {
                            sFiller1 = "0" + sFiller1;
                        }
                        string sTeamkod = w.sTeamkod;
                        while (sTeamkod.Length < 7)
                        {
                            sTeamkod = "0" + sTeamkod;
                        }
                        file.WriteLine(sTeamkod + sFiller1 + record.sDatum + record.sLooncode + record.iUren.ToString("0000") + "             " + record.sKp);
                    }
                    else if (w.sFiller1 == string.Empty && w.sSalif == "1" && lsSalnums.Contains(w.sSalnum))
                    {
                        Blox.sf.schrijfUitzondering("Salarisnummer niet ingevuld bij" + w.sSalnum,string.Empty);
                    }
                    
                }
                
            }
            Log.Verwerking(Record.lrRecords, "Records weggeschreven");

            file.Close();
        }
        public void schrijfUitzondering(string sProbleem, string sRecord)
        {
            StreamWriter sw = File.AppendText(Parameter.SSbRoot + @"log\SalinfLog\\" + sUitzonderingFileNaam);
            sw.WriteLine(sProbleem + sRecord);
            sw.Close();
        }
        #endregion

    }
}
