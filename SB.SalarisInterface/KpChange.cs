﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SB.SalarisInterface
{
    public class KpChange
    {
        #region Fields
        int iDate;
        string sKeyValue;
        string sNewValue;
        public static List<KpChange> lkcChanges = new List<KpChange>();
        public static Dictionary<string, string> dWplek = new Dictionary<string, string>();
        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public KpChange(int iDate,string sKeyValue,string sNewValue)
        {
            this.iDate = iDate;
            this.sKeyValue = sKeyValue;
            this.sNewValue = sNewValue;
        }

        #endregion

        #region Properties
        public string SKp
        {
            get
            {
                return this.sNewValue;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sorteer lkcChanges zodoende steeds de laatste wijziging te krijgen
        /// </summary>
        public static void SortLkcChanges()
        {
            lkcChanges.Sort(delegate(KpChange kp1, KpChange kp2) { return kp2.iDate.CompareTo(kp1.iDate); });
        }

        /// <summary>
        /// zoek de laatste change tbv Werknemer.SGeefKpWerknemer
        /// </summary>
        /// <param name="iVoorDatum"></param>
        /// <param name="sBadnum"></param>
        /// <returns></returns>
        public static KpChange KCZoekLaatsteChange(int iVoorDatum, string sBadnum)
        {
             KpChange kc = lkcChanges.Find(
             delegate(KpChange kcTmp)
             {
                 DateTime oDate = SB.Algemeen.Proces.dtStringToDate(kcTmp.iDate.ToString());
                 DateTime iDate = SB.Algemeen.Proces.dtStringToDate(iVoorDatum.ToString());
                 return (oDate <= iDate) && (kcTmp.sKeyValue == sBadnum);
             }
             );
             return kc;
        }
        #endregion

    }
}
