﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SB.SalarisInterface
{
    public class Werknemer
    {
        #region Fields
        public string sBadnum, sSalnum, sKp, sFiller1, sMandant, sAhvnr, sFunktie, sWochsoll, sFiller2, sTeamkod, sAfdkod, sMa_grp, sSalif;

        public static List<Werknemer> lwMedewerkers = new List<Werknemer>();
        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Werknemer(string sBadnum, string sSalnum, string sKp,string sFiller1,string sMandant,string sAhvnr,string sFunktie,string sWochsoll,string sFiller2,string sTeamkod,string sAfdkod,string sMa_grp,string sSalif)
        {
            this.sBadnum = sBadnum;
            while (sSalnum.Length < 7)
                sSalnum = "0" + sSalnum;
            if (sSalnum.Length > 7 && sSalif == "1")
                Blox.lsSalnumsTeLang.Add(sSalnum);
            while (sSalnum.Length > 7)
                sSalnum = sSalnum.Substring(1);
            this.sSalnum = sSalnum;
            this.sKp = sKp;
            this.sFiller1 = sFiller1;
            this.sMandant = sMandant;
            this.sAhvnr = sAhvnr;
            this.sWochsoll = sWochsoll;
            this.sFiller2 = sFiller2;
            this.sTeamkod = sTeamkod;
            this.sAfdkod = sAfdkod;
            this.sMa_grp = sMa_grp;
            this.sSalif = sSalif;
        }

        #endregion

        #region Properties

        #endregion

        #region Methods
        public static Werknemer WZoekWerknemer(string sSalnum)
        {
            Werknemer result = lwMedewerkers.Find(
            delegate(Werknemer w)
            {
                return w.sSalnum == sSalnum;
            }
            );
            return result;
        }
        /// <summary>
        /// Geef de laatste kp van de werknemer op de gegeven datum (yyyyMMdd)
        /// </summary>
        /// <param name="sSalnum"></param>
        /// <param name="iDatum"></param>
        /// <returns></returns>
        public static string SGeefKpWerknemer(string sSalnum, int iDatum)
        {
            while (sSalnum.Length < 7)
            {
                sSalnum = "0" + sSalnum;
            }
            
            Werknemer w = WZoekWerknemer(sSalnum);
            if (w != null)
            {
                KpChange kc = KpChange.KCZoekLaatsteChange(iDatum, w.sBadnum);
                if (kc != null)
                {
                    if (KpChange.dWplek.ContainsKey(kc.SKp))
                    {
                        return KpChange.dWplek[kc.SKp];
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    if (KpChange.dWplek.ContainsKey(w.sKp))
                    {
                        return KpChange.dWplek[w.sKp];
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion

    }
}
