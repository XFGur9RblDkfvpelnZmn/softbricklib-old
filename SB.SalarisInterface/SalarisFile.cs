﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SB.Algemeen;

namespace SB.SalarisInterface
{
    public class SalarisFile
    {
        #region Fields

        protected string sOrigineelFileNaam;
        public string sResultaatFileNaam;
        public string sUitzonderingFileNaam;
        public string sInhoud;
        public string sTimeStamp;

        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public SalarisFile(string sOrigineelFileNaam, string sResultaatFileNaam,string sUitzonderingFileNaam)
        {
            //new Proces(sMaakSalarisFileCommando);
            this.sOrigineelFileNaam = sOrigineelFileNaam;
            this.sResultaatFileNaam = sResultaatFileNaam;
            
            this.sTimeStamp = DateTime.Now.ToString("yyyyMMddHHmmssff");
            this.sUitzonderingFileNaam = sUitzonderingFileNaam+sTimeStamp;
            this.KopieerFile();
            this.sInhoud = sLeesInhoud();
        }

        #endregion

        #region Properties

        #endregion

        #region Methods

        public void OpslaanFile()
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(Parameter.SSbRoot + "log\\" + this.ToString());
                sw.Write(this.sInhoud);
                Log.Verwerking(this, "Wijzigingen opgeslagen");
            }
            catch (SystemException ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }

        }
        public void KopieerFile()
        {
            try
            {                
                File.Copy(Parameter.SSbRoot + "log\\" + this.sOrigineelFileNaam, Parameter.SSbRoot + "log\\SalinfLog\\" + this.sOrigineelFileNaam + sTimeStamp);
                Log.Verwerking(this, "Origineel bestand gelogd : " + this.sOrigineelFileNaam+sTimeStamp);
                File.Copy(Parameter.SSbRoot + "log\\" + this.sOrigineelFileNaam, Parameter.SSbRoot + "log\\SalinfLog\\" + this.sResultaatFileNaam + sTimeStamp);
                Log.Verwerking(this, "Origineel bestand gekopieerd voor verwerking : " + this.sResultaatFileNaam + sTimeStamp);
            }
            catch (DirectoryNotFoundException ex)
            {
                Directory.CreateDirectory(Parameter.SSbRoot + "log\\SalinfLog");
                KopieerFile();
                Log.Exception(ex);
                Log.Verwerking("Directory SalinfLog aangemaakt");
            }
            catch (SystemException ex)
            {
                Log.Exception(ex);
            }

        }
        protected string sLeesInhoud()
        {
            string sInhoud = Proces.SLeesFile(Parameter.SSbRoot+"log\\SalinfLog\\"+this.ToString());
            return sInhoud;
        }
        public override string ToString()
        {
            return this.sResultaatFileNaam+sTimeStamp;
        }

        #endregion

    }
}
