﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEA.FeestdagRecht
{
    class Dagschema
    {
        #region Fields
        string sNummer;
        public string sDagschematijd,sBegin,sEind,sNominaaltijd;

        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Dagschema(string sNummer, string sDagschematijd,string sBegin,string sEind,string sNominaaltijd)
        {
            this.sNummer = sNummer;
            this.sDagschematijd = sDagschematijd;
            this.sBegin = sBegin;
            this.sEind = sEind;
            this.sNominaaltijd = sNominaaltijd;
        }

        #endregion

        #region Properties

        #endregion

        #region Methods
        public static Dagschema zoekDs(string sNummer, List<Dagschema> ldsLijst)
        {
            Dagschema returnDs = ldsLijst.Find(delegate(Dagschema ds) { return ds.sNummer == sNummer; });
            return returnDs;
        }
        #endregion

    }
}
