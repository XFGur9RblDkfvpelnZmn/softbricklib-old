﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEA.FeestdagRecht
{
    class Persoon
    {
        #region Fields
        public string sSalnum;
        public List<string> lsDagschema = new List<string>();
        public List<string> lsDagschemaTijd = new List<string>();
        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Persoon(string sSalnum)
        {
            this.sSalnum = sSalnum;
        }

        #endregion

        #region Properties

        #endregion

        #region Methods
        public static Persoon zoekPs(string sNummer, List<Persoon> lpLijst)
        {
            Persoon returnPs = lpLijst.Find(delegate(Persoon ps) { return ps.sSalnum == sNummer; });
            return returnPs;
        }
        #endregion

    }
}
