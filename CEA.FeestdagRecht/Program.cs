﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.Algemeen;
using System.IO;
using System.Text.RegularExpressions;

namespace CEA.FeestdagRecht
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Persoon> lpLijst = new List<Persoon>();
            List<Dagschema> ldsLijst = new List<Dagschema>();
            List<string> lsFeestdagen = new List<string>();

            List<String> lsArgs = new List<string>();
            lsArgs.Add("-d;");

            string sInhoudFeestdag;
            string sInhoudDagschema;
            string sInhoudDagschemaPlanning;


            HaalSbGegevensOp(args, lsArgs, out sInhoudFeestdag, out sInhoudDagschema, out sInhoudDagschemaPlanning);


            foreach (string sLijn in Regex.Split(sInhoudFeestdag, Environment.NewLine))
            {
                if (sLijn.Trim().Length > 0)
                {
                    if (sLijn.Split(';')[1].Trim() == "1")
                    {
                        Int32 result;
                        string jaar = DateTime.Now.Year.ToString();
                        if (args[0].Contains("this_year_start+1"))
                        {
                            jaar = (DateTime.Now.Year+1).ToString();
                        }
                        else if (args[0].Contains("this_year"))
                        {
                            jaar = DateTime.Now.Year.ToString();
                        }
                        else if(Int32.TryParse(args[0],out result))
                        {
                            jaar = args[0].Substring(0, 4);

                        }
                        lsFeestdagen.Add(jaar + sLijn.Split(';')[0].Substring(4));
                    }
                    else
                    {
                        lsFeestdagen.Add(sLijn.Split(';')[0]);
                    }
                }
            }

            foreach (string sLijn in Regex.Split(sInhoudDagschema, Environment.NewLine))
            {
                if (sLijn.Trim().Split(';').Length >= 4)
                {
                    Dagschema ds = new Dagschema(sLijn.Split(';')[0], sLijn.Split(';')[1], sLijn.Split(';')[2], sLijn.Split(';')[3],sLijn.Split(';')[4]);
                    ldsLijst.Add(ds);
                }
            }
            //maak hhopla import file
            StreamWriter swHhoplaImportFile = new StreamWriter(SB.Algemeen.Parameter.SSbRoot + "log\\feestdag.imp.csv");
            foreach (string sLijn in Regex.Split(sInhoudDagschemaPlanning, Environment.NewLine))
            {
                if (sLijn.Split(';')[0] != "salnum"&&sLijn.Trim().Length>0)
                {
                    DateTime dtDatum = new DateTime(Convert.ToInt32(sLijn.Split(';')[1].Substring(0, 4)), Convert.ToInt32(sLijn.Split(';')[1].Substring(4, 2)), Convert.ToInt32(sLijn.Split(';')[1].Substring(6, 2)));
                    
                    //zoek of persoon reeds bestaat
                    Persoon pe = Persoon.zoekPs(sLijn.Split(';')[0].Trim(), lpLijst);
                    if (pe == null)
                    {
                        //maak indien nodig persoon aan
                        pe = new Persoon(sLijn.Split(';')[0].Trim());
                        lpLijst.Add(pe);
                    }
                    if (pe.sSalnum == "0800633")
                    {
                    }
                    //voor alle feestdagen die niet in het weekend vallen
                    if (((dtDatum.DayOfWeek != DayOfWeek.Sunday) && (dtDatum.DayOfWeek != DayOfWeek.Saturday)) && (lsFeestdagen.IndexOf(sLijn.Split(';')[1]) > -1))
                    {
                        //voeg dagschema toe aan persoon
                        pe.lsDagschema.Add(sLijn.Split(';')[2]);
                        try
                        {
                            FileInfo fiPersoon = Proces.FiSqlsel("filler2,kenteken from personeel where salnum = '" + pe.sSalnum + "' and sdatp <= " + dtDatum.ToString("yyyyMMdd") + " and edatp >= " + dtDatum.ToString("yyyyMMdd"), lsArgs);
                            string sPersoon = Proces.SLeesFile(fiPersoon.FullName);
                            if (sPersoon.Trim() != string.Empty)
                            {
                                Dagschema ds = Dagschema.zoekDs(sLijn.Split(';')[2], ldsLijst);
                                pe.lsDagschemaTijd.Add(ds.sDagschematijd);
                                
                                int iBeginTijdUur = Convert.ToInt32(ds.sBegin.Trim())/60;
                                int iBeginTijdMin = Convert.ToInt32(ds.sBegin.Trim())%60;
                                int iEindTijdUur = (Convert.ToInt32(ds.sBegin) + Convert.ToInt32(ds.sNominaaltijd)) / 60;
                                int iEindTijdMin = (Convert.ToInt32(ds.sBegin) + Convert.ToInt32(ds.sNominaaltijd)) % 60;
                                if(iBeginTijdMin+iBeginTijdUur+iEindTijdMin+iEindTijdUur!=0)
                                    swHhoplaImportFile.WriteLine(pe.sSalnum + ";" + dtDatum.ToString("yyyyMMdd") + ";" + args[3] + ";" + iBeginTijdUur.ToString("00") + iBeginTijdMin.ToString("00") + ";" + iEindTijdUur.ToString("00") + iEindTijdMin.ToString("00") + ";;;;");
                            }
                        }
                        catch (NullReferenceException ex)
                        {
                            //dagschema bestaat niet
                        }
                    }
                    else if(lsFeestdagen.IndexOf(sLijn.Split(';')[1]) > -1)
                    {
                        lsArgs.Clear();
                        lsArgs.Add("-d;");
                        lsArgs.Add("-c"+dtDatum.ToString("yyyyMMdd"));
                        lsArgs.Add("-s");
                        FileInfo fiPersoon = Proces.FiSqlsel("filler2,kenteken from personeel where salnum = '" + pe.sSalnum + "' and sdatp <= "+dtDatum.ToString("yyyyMMdd")+" and edatp >= "+dtDatum.ToString("yyyyMMdd"),lsArgs);
                        string sPersoon = Proces.SLeesFile(fiPersoon.FullName);
                        string[] asData = sPersoon.Split(';');
                        int iContract,iDagen,iTijd;

                        try
                        {
                            iContract = Convert.ToInt32(asData[0]);
                            iDagen = Convert.ToInt32(asData[1]);
                            if (iDagen == 0)
                            {
                                iDagen = 4;
                            }
                            iTijd = iContract / iDagen;
                        }
                        catch (Exception ex)
                        {
                            iTijd = 0;
                        }
                        pe.lsDagschemaTijd.Add(iTijd.ToString());
                    }
                }
            }
            swHhoplaImportFile.Close();
            StreamWriter sw = new StreamWriter(args[2]);
            foreach (Persoon p in lpLijst)
            {
                sw.Write(p.sSalnum + ";");
                int i = 0;
                foreach (string s in p.lsDagschemaTijd)
                {
                    if (s.Trim() != "")
                        i += Convert.ToInt32(s.Trim());
                }
                sw.Write(i.ToString() + Environment.NewLine);
            }
            sw.Close();
            new Proces("clnt_exp imp_aanvraag_cena feestdag.imp.csv 30");
            Proces.VerwijderWrkFiles();
        }

        private static void HaalSbGegevensOp(string[] args, List<String> lsArgs, out string sInhoudFeestdag, out string sInhoudDagschema, out string sInhoudDagschemaPlanning)
        {
            try
            {
                FileInfo fiFeestdag = Proces.FiSqlsel("datum, jaarlijks from vakantie", lsArgs);
                FileInfo fiDagschema = Proces.FiSqlsel("schkod, psna,nin1,nuit1,nomd1 from dagschema", lsArgs);
                FileInfo fiDagschemaPlanning = Proces.FiExpDagschemaCena(args[0], args[1]);

                sInhoudFeestdag = Proces.SLeesFile(fiFeestdag.FullName);
                sInhoudDagschema = Proces.SLeesFile(fiDagschema.FullName);
                sInhoudDagschemaPlanning = Proces.SLeesFile(fiDagschemaPlanning.FullName);
            }
            catch (FileNotFoundException ex)
            {
                sInhoudFeestdag = string.Empty;
                sInhoudDagschema = string.Empty;
                sInhoudDagschemaPlanning = string.Empty;
                SB.Algemeen.Log.Exception(ex);
                //HaalSbGegevensOp(args, lsArgs, out sInhoudFeestdag, out sInhoudDagschema, out sInhoudDagschemaPlanning);
            }
        }
    }
}
