﻿using System;
using System.Collections.Generic;
using System.Text;
using SB.PeriodeOverzicht;
using System.IO;
using SB.Algemeen;
using SB.SalarisInterface;
using System.Collections;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Diagnostics;
namespace SB.TestLib
{
    public class Program
    {
        
        static void Main(string[] args)
        {
            
            try
            {
                Log.Verwerking("Start Salarisverwerking");
                while (IsProcessOpen("blox"))
                {
                    Log.Verwerking("wacht op proces");
                    System.Threading.Thread.Sleep(100);
                }
                //LEES BLOX BESTAND
                Blox.sf = new Blox("expblox1.txt", "expblox_verw.Txt");
                int iStartDat=29999999;
                int iEindDat=0;
                foreach (string sLijn in Regex.Split(Blox.sf.sInhoud, Environment.NewLine))
                {
                    if (sLijn.Length > 28)
                    {
                        if (Convert.ToInt32(sLijn.Substring(14, 6)) < iStartDat)
                        {
                            iStartDat = Convert.ToInt32(sLijn.Substring(14, 6));
                        }
                        else if (Convert.ToInt32(sLijn.Substring(14, 6)) > iEindDat)
                        {
                            iEindDat = Convert.ToInt32(sLijn.Substring(14, 6));
                        }

                    }
                }
                
                //DEFINIEER ARGUMENTEN
                DateTime dtBeginDat = Proces.dtStringToDate(iStartDat.ToString());
                DateTime dtEindDat = Proces.dtStringToDate(iEindDat.ToString());

                //SELECTEER PERSONEEL
                SelecteerPersoneel(dtBeginDat);

                //SELECTEER KP'S
                SelecteerWerkplekken(dtBeginDat);

                //SELECTEER KOSTENPLAATS WISSELINGEN
                SelecteerKpChanges(dtBeginDat, dtEindDat);

                //MAAK WOCHSOLL FILES
                Wochsoll.MaakWochsollFiles(dtBeginDat, dtEindDat);
                #region weeknominalen

                // CHECK HOEVEEL WEKEN IN JAAR
                DateTime dDatum = new DateTime(DateTime.Now.Year, 12, 31);
                //BEREKEN WEEK VH JAAR
                CultureInfo myCI = new CultureInfo("nl-BE");
                Calendar myCal = myCI.Calendar;

                // Gets the DTFI properties required by GetWeekOfYear.
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
                // WEEK VH JAAR
                int week = myCal.GetWeekOfYear(dDatum, myCWR, myFirstDOW);



                // VOOR ELKE WEEK VAN HET JAAR
                for (int i = 1; i <= week; i++)
                {
                    // MAAK EEN FILE AAN VOOR ELKE WEEK VH JAAR
                    string sInhoud = Proces.SLeesFile(Parameter.SSbRoot + @"log\Wochsoll\week" + Convert.ToString(week) + ".wrk");

                    foreach (string line in Regex.Split(sInhoud, Environment.NewLine))
                    {
                        // STEEK DE DATA IN EEN OBJECT WOCHSOLL EN VOEG TOE AAN ARRAYLIST
                        string[] data = line.Split(';');
                        if ((data[0].Trim().Length > 0) && (data[1].Trim().Length > 0) && (data[2].Trim().Length > 0))
                        {
                            Wochsoll nieuw = new Wochsoll(data[0].Trim(), Convert.ToInt32(data[1].Trim()), Convert.ToInt32(data[2].Trim()), i);
                            Wochsoll.lwWochsoll.Add(nieuw);
                        }

                    }
                }

                #endregion

                //SELECTEER DAGSCHEMA'S EN VOEG IN DICTIONARY
                Dictionary<string, int> dDagschema = dSelecteerDagschema();

                //SELECTEER WELKE DAGSCHEMA'S WANNEER GEPLAND ZIJN EN LEES INHOUD IN STRING
                FileInfo fi = Proces.FiExpDagschemaCena(dtBeginDat.ToString("yyMMdd"), dtEindDat.ToString("yyMMdd"));
                string sInhoudDagschema = Proces.SLeesFile(fi.FullName);

                //OVERLOOP PLANNING EN MAAK RECORDS AAN PER DAG
                OverloopPlanning(dDagschema, sInhoudDagschema);

                

                //OVERLOOP BLOX BESTAND EN VERVANG IN DS
                foreach (string sLijn in Regex.Split(Blox.sf.sInhoud, Environment.NewLine))
                {
                    if (sLijn.Length > 28)
                    {
                        string sLooncode = sLijn.Substring(20, 4);
                        string sSalarisNr = sLijn.Substring(7, 7);
                        if (!Blox.lsSalnums.Contains(sSalarisNr))
                        {
                            Blox.lsSalnums.Add(sSalarisNr);
                        }
                        if ((sLooncode != "0000") && (sLooncode != "1010"))
                        {
                            //  SPLITS LIJN IN DATA
                            string sWerkgeverNr = sLijn.Substring(0, 7);
                            
                            string sDatum = sLijn.Substring(14, 6);
                            Int32 iUren = Convert.ToInt32(sLijn.Substring(24, 4));
                            
                            Record r = Record.RZoekRecord(sSalarisNr, sDatum, "1010");
                            if (sLooncode == "1041" || sLooncode == "1046")
                            {
                                Record rNieuw = new Record(sSalarisNr, Parameter.SWerkgeverNr, sDatum, sLooncode, iUren, Werknemer.SGeefKpWerknemer(sSalarisNr, Convert.ToInt32(sDatum)));
                                Record.lrRecords.Add(rNieuw);
                            }
                            else if (r == null)
                            {
                                Blox.sf.schrijfUitzondering("Werknemer/1010 record niet gevonden", sLijn);
                            }
                            else if (iUren <= r.iUren)
                            {
                                r.iUren -= iUren;
                                Record rNieuw = new Record(sSalarisNr, Parameter.SWerkgeverNr, sDatum, sLooncode, iUren, Werknemer.SGeefKpWerknemer(sSalarisNr, Convert.ToInt32(sDatum)));
                                Record.lrRecords.Add(rNieuw);
                            }
                            else if ((iUren > r.iUren)&&(sLooncode != "9900"))
                            {
                                r.sLooncode = sLooncode;
                                iUren = iUren - r.iUren;
                                sLijn.Remove(24,4);
                                sLijn.Insert(24,iUren.ToString("0000"));
                                Blox.sf.schrijfUitzondering("Te weinig dagschema-uren voor looncode", sLijn);
                            }
                        }
                    }
                }


                //  ARRAYLIST VOOR TOEVOEGEN NIEUWE RECORDS
                ArrayList newRec = new ArrayList();

                //  VARS OM TE CHECKEN OF BIJLAGECONTRACT VOOR DIE WEEK REEDS BEREKEND IS
                int lastWeekBijlage = 0;
                string lastSalnumBijlage = "";
                int iVerwerkteRecs = 0;
                //  VOOR ELK OBJECT IN RECORDS
                foreach (Record record in Record.lrRecords)
                {
                    iVerwerkteRecs++;
                    record.checkDubbel(Record.lrRecords);
                    //  ZOEK DE JUISTE PERSOON BIJ HET RECORD
                    Werknemer persoon = Werknemer.WZoekWerknemer(record.sSalnum);

                    //  ZET UREN OP 0 BIJ LOONCODE 9450 en 9998
                    if ((record.sLooncode == "9450") || (record.sLooncode == "9998"))
                    {
                        record.geenUren();
                    }
                    //  WANNEER LOONCODE 1013of1014 zet 1010 UREN OM
                    if ((record.sLooncode == "1013") || (record.sLooncode == "1014") || (Convert.ToInt32(record.sLooncode) >= 1042) && (Convert.ToInt32(record.sLooncode) <= 1044))
                    {
                        record.zet1013Om(Record.lrRecords, persoon);
                    }
                    //  VERANDER LOONCODE BIJ BIJLAGECONTRACT
                    int bijlage = Wochsoll.checkBijlageContract(record.week, record.sSalnum);
                    if ((bijlage > 0) && ((lastWeekBijlage != record.week) || (lastSalnumBijlage != persoon.sSalnum)))
                    {
                        Record newCheck = record.checkLooncode(persoon, bijlage);
                        if (newCheck.sLooncode != "")
                        {
                            newRec.Add(newCheck);
                        }
                        lastSalnumBijlage = persoon.sSalnum;
                        lastWeekBijlage = record.week;
                    }
                    // HAAL CODE 9876 WEG EN HAAL EVENVEEL 1010 uren WEG.
                    if (record.sLooncode == "9876")
                    {
                        record.removeUren(persoon);
                    }
                    // WANNEER LOONCODE 1041 OP ZONDAG VOORKOMT, ZET DEZE OP EEN ANDERE DAG EN HAAL 1010 weg
                    else if (((record.sLooncode == "1041") || (record.sLooncode == "1046")) && (record.dtDatum.DayOfWeek == DayOfWeek.Sunday))
                    {
                        newRec.AddRange(record.omBoek1041(persoon));
                    }
                    //  MAX. 8u OP LOONCODE 1049
                    //Record newChecks = record.checkLooncode1049(persoon);
                    //if (newChecks.sLooncode != "")
                    //{
                    //    newRec.Add(newChecks);
                    //}

                    //if (persoon.Mandant == "FIL")
                    {
                        //Record newCheck = record.checkRoulement(persoon, records);
                        record.checkRoulement(persoon);
                        //if (newCheck.LoonCode != "")
                        //{
                        //    newRec.Add(newCheck);
                        //}
                    }

                }
                // NIEUWE RECORDS TOEVOEGEN AAN ARRAYLIST
                foreach (Record r in newRec)
                {
                    Record.lrRecords.Add(r);
                }

                foreach (Record record in Record.lrRecords)
                {
                    Werknemer persoon = Werknemer.WZoekWerknemer(record.sSalnum);
                    /*if (persoon.sMandant == "FIL")
                    {
                        //  VERANDER KOSTENPLAATS
                        record.checkKp(persoon, filKp);
                    }
                    else
                    {
                        record.checkContract(persoon, records);
                        //  VERANDER KOSTENPLAATS DC
                        record.checkKp(persoon, disKp);
                    }*/
                    // DUBBELE RECORDS VERWIJDEREN (bv 0-records)
                    record.checkDubbel(Record.lrRecords);
                    // LOONCODE 1010 (of 0000) MET 0000 UREN ENKEL LATEN STAAN INDIEN GEEN ANDER RECORD OP ZELFDE DAG
                    if (((record.sLooncode == "1010") || (record.sLooncode == "0000")) && (record.iUren == 0) && (record.bestaatRecordZelfdeDag()))
                    {
                        record.sSalnum = "weg";
                    }
                }

                Blox.sf.writeFile();
                

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                try
                {
                    Proces.Tc_lp("log\\SalinfLog\\" + Blox.sf.sUitzonderingFileNaam);
                    System.Threading.Thread.Sleep(4000);
                    Proces.VerwijderWrkFiles();
                    Log.Verwerking("Einde Salarisverwerking");
                    Proces.Tc_lp("log\\SalinfLog\\" + Blox.sf.sResultaatFileNaam + Blox.sf.sTimeStamp);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }
            }
        }

        
        #region methodes
        private static void SelecteerWerkplekken(DateTime dtBeginDat)
        {
            List<string> liWplekArgs = new List<string>();
            liWplekArgs.Add("-d;"); // splits velden op ;
            liWplekArgs.Add("-c" + dtBeginDat.ToString("yyyyMMdd")); //selecteer de gegevens geldig op de begindatum
            FileInfo fiWplek = Proces.FiSqlsel("wplekkod, filler from werkplek", liWplekArgs);
            string sInhoudWplek = Proces.SLeesFile(fiWplek.FullName);
            
            foreach (string sLijn in Regex.Split(sInhoudWplek, Environment.NewLine))
            {
                string[] asData = sLijn.Split(';');
                
                if (asData.Length == 3)
                {
                    KpChange.dWplek.Add(asData[0].Trim(), asData[1].Trim());
                }
            }
        }

        public static bool IsProcessOpen(string name)
        {
            //here we're going to get a list of all running processes on
            //the computer
            foreach (Process clsProcess in Process.GetProcesses())
            {
                //now we're going to see if any of the running processes
                //match the currently running processes. Be sure to not
                //add the .exe to the name you provide, i.e: NOTEPAD,
                //not NOTEPAD.EXE or false is always returned even if
                //notepad is running.
                //Remember, if you have the process running more than once, 
                //say IE open 4 times the loop thr way it is now will close all 4,
                //if you want it to just close the first one it finds
                //then add a return; after the Kill
                if (clsProcess.ProcessName.Contains(name))
                {
                    //if the process is found to be running then we
                    //return a true
                    return true;
                }
            }
            //otherwise we return a false
            return false;
        }

        private static void OverloopPlanning(Dictionary<string, int> dDagschema, string sInhoudDagschema)
        {
            foreach (string sLijn in Regex.Split(sInhoudDagschema, Environment.NewLine))
            {
                //INDIEN LIJN NIET LEEG IS
                if (sLijn.Trim().Length > 0)
                {
                    String[] asData = sLijn.Split(';');
                    int iTmp;
                    //INDIEN HET DAGSCHEMA GEVONDEN WORDT
                    if (dDagschema.ContainsKey(asData[2])&&dDagschema[asData[2]] != 0)
                    {
                        //RECORD AANMAKEN MET DAGSCHEMATIJD
                        string sKp = Werknemer.SGeefKpWerknemer(asData[0].Trim(), Convert.ToInt32(asData[1].Trim()));
                        Record r = new Record(asData[0].Trim(), Parameter.SWerkgeverNr, asData[1].Trim().Substring(2), "1010", dDagschema[asData[2]], sKp);
                        Record.lrRecords.Add(r);
                        
                    }
                    //INDIEN DAGSCHEMA NIET BESTAAT
                    else if (int.TryParse(asData[0].Trim(), out iTmp))
                    {
                        //LEEG RECORD AANMAKEN
                        string sKp = Werknemer.SGeefKpWerknemer(asData[0].Trim(), Convert.ToInt32(asData[1].Trim()));
                        Record r = new Record(asData[0].Trim(), Parameter.SWerkgeverNr, asData[1].Trim().Substring(2), "0000", 0, sKp);
                        Record.lrRecords.Add(r);
                    }
                }
            }
        }

        private static Dictionary<string, int> dSelecteerDagschema()
        {
            //SELECTEER DAGSCHEMA'S EN LEES BESTAND IN
            List<string> lsArgs = new List<string>();
            lsArgs.Add("-d;"); //splits op ;
            FileInfo fiSql = Proces.FiSqlsel("schkod, nomd1 from dagschema", lsArgs);
            Log.Verwerking(fiSql, "Dagschema file aangemaakt");
            string sInhoud = Proces.SLeesFile(fiSql.FullName);

            //VOEG DAGSCHEMA'S IN DICTIONARY
            Dictionary<string, int> dDagschema = new Dictionary<string, int>();
            foreach (string sLijn in Regex.Split(sInhoud, Environment.NewLine))
            {
                if (sLijn.Split(';').Length == 3)
                {
                    String[] asData = sLijn.Split(';');
                    dDagschema.Add(asData[0].Trim(), Convert.ToInt32(Convert.ToDecimal(asData[1].Trim()) / 60m * 100)); //tijd in min omrekenen in hondersten van uren
                }
            }
            return dDagschema;
        }

        private static void SelecteerKpChanges(DateTime dtBeginDat, DateTime dtEindDat)
        {
            //DEFINIEER ARGUMENTEN VOOR SELECTIE KPCHANGES
            List<string> liDbChangeArgs = new List<string>();
            liDbChangeArgs.Add("-d;"); // splits velden op ;
            liDbChangeArgs.Add("-c" + dtEindDat.ToString("yyyyMMdd")); //selecteer de gegevens geldig op de einddatum

            //SELECTEER DBCHANGERECORDS EN VOEG INHOUD IN STRING
            FileInfo fiDbChange = Proces.FiSqlsel("date,key_value,new_value from dbchng where date > " + dtBeginDat.ToString("yyyyMMdd") + " and date < " + dtEindDat.ToString("yyyyMMdd") + " and set = 'personeel' and field = 'wplekkod'", liDbChangeArgs);
            Log.Verwerking(fiDbChange, "DBCHANG File aangemaake");
            string sInhoudKpChange = Proces.SLeesFile(fiDbChange.FullName);

            //LEES DBCHANGERECORDS EN VOEG ALLE CHANGES IN DE STATIC LIST lkcChanges
            foreach (string sLijn in Regex.Split(sInhoudKpChange, Environment.NewLine))
            {
                if (sLijn.Split(';').Length == 4)
                {
                    string[] asData = sLijn.Split(';');
                    KpChange kc = new KpChange(Convert.ToInt32(asData[0].Trim()), asData[1].Trim(), asData[2].Trim());
                    KpChange.lkcChanges.Add(kc);
                }
            }

            //SORTEER KP CHANGES OP DATUM ZODAT STEEDS DE LAATSTE CHANGE GENOMEN WORDT
            KpChange.SortLkcChanges();
        }

        private static void SelecteerPersoneel(DateTime dtBeginDat)
        {
            //DEFINIEER ARGUMENTEN VOOR SELECTIE PERSONEEL
            List<string> liPersArgs = new List<string>();
            liPersArgs.Add("-d;"); // splits velden op ;
            liPersArgs.Add("-c" + dtBeginDat.ToString("yyyyMMdd")); //selecteer de gegevens geldig op de begindatum

            //SELECTEER PERSONEEL EN VOEG INHOUD IN STRING
            FileInfo fiPersoneel = Proces.FiSqlsel("badnum, salnum, wplekkod,filler1,mandant,ahvnr,funktie,wochsoll,filler2,teamkod,afdkod,ma_grp,salif from personeel", liPersArgs);
            Log.Verwerking(fiPersoneel, "Personeel File aangemaake");
            string sInhoudPersoneel = Proces.SLeesFile(fiPersoneel.FullName);

            //LEES ALLE WERKNEMERS IN EN VOEG DEZE IN DE STATIC LIJST LWMEDEWERKERS
            foreach (string sLijn in Regex.Split(sInhoudPersoneel, Environment.NewLine))
            {
                if (sLijn.Split(';').Length == 14)
                {
                    string[] asData = sLijn.Split(';');
                    
                    
                    Werknemer w = new Werknemer(asData[0].Trim(), asData[1].Trim(), asData[2].Trim(), asData[3].Trim(), asData[4].Trim(), asData[5].Trim(), asData[6].Trim(), asData[7].Trim(), asData[8].Trim(), asData[9].Trim(), asData[10].Trim(), asData[11].Trim(), asData[12].Trim());
                    Werknemer.lwMedewerkers.Add(w);
                }
            }
        }

        

        #endregion
    }
}
