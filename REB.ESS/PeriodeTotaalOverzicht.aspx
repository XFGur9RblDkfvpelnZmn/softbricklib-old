﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PeriodeTotaalOverzicht.aspx.cs" Inherits="REB.ESS.PeriodeTotaalOverzicht" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="Form1" runat="server">
        <center>
		    <img src="img/brantano.jpg" style="width:316px;height:119px;" alt="" /><br /><br />
		    <div style="min-width:450px;height:50px;border:0px solid black;text-align:center;margin-bottom:-10px;">
			    <a href="Default.aspx">Personeelsgegevens</a> | <a href="PeriodeTotaalOverzicht.aspx">PeriodeTotaalOverzicht</a> 
		    </div>
		    <div style="width:350px;border:1px solid black;text-align:left; padding: 5px">
		
		    
				    <h2>Overzichten</h2>
                    Van:
                    <asp:Calendar  ID="cdStartDatum" runat="server" BackColor="White" 
                        BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                        Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" 
                        Width="200px" >
                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                        <NextPrevStyle VerticalAlign="Bottom" />
                        <OtherMonthDayStyle ForeColor="#808080" />
                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                        <SelectorStyle BackColor="#CCCCCC" />
                        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <WeekendDayStyle BackColor="#FFFFCC" />
                    </asp:Calendar>
                    Tot: <asp:Calendar ID="cdEindDatum" runat="server" BackColor="White" 
                        BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                        Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" 
                        Width="200px" >
                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                        <NextPrevStyle VerticalAlign="Bottom" />
                        <OtherMonthDayStyle ForeColor="#808080" />
                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                        <SelectorStyle BackColor="#CCCCCC" />
                        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <WeekendDayStyle BackColor="#FFFFCC" />
                    </asp:Calendar>
                    Tot: 
				    <asp:DropDownList ID="ddlSelectie" runat="server" onload="ddlSelectie_Load" 
                        onselectedindexchanged="ddlSelectie_SelectedIndexChanged" AutoPostBack="true" />
                    <asp:DropDownList ID="ddlSelopp" runat="server" onload="ddlSelopp_Load" />
                    <asp:ListBox ID="lbSelectie" runat="server" SelectionMode="Multiple" />
                    <!--<asp:TextBox ID="txtSelectie" runat="server" />-->
                     <asp:TextBox ID="txtSel" runat="server" Visible="false" />
                    <asp:DropDownList ID="ddlOverzichten" runat="server" 
                        onload="ddlOverzichten_Load" />
                    <!--<asp:TextBox ID="salnum" runat="server" Enabled="false" />
				        <br />			-->	
                     <asp:Button Text="Verzenden" runat="server" ID="btnSubmit"  
                     />
                     


		    </div>
            <div id="divResult" runat="server">
            </div>
	    </center>
    </form>
</body>

</html>
