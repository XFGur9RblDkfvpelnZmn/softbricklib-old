﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SB.Algemeen;

namespace REB.ESS
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Dictionary<string, List<string>> dlsLogin = Proces.DlsSqlSel("salnum,kenteken from personeel where salnum = '" + Request["login"] + "'", DateTime.Now);
                if (Request["login"] != null && Request["pass"] != null && dlsLogin.Count > 0 && dlsLogin[Request["login"].ToString().Trim()][0] == Request["pass"].ToString().Trim())
                {
                    Session["login"] = Request["login"];
                    Session["pass"] = Request["pass"];
                    Response.Redirect("Default.aspx");
                }
            }
            
        }
    }
}