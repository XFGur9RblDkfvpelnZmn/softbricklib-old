﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SB.Algemeen;
using System.IO;
namespace REB.ESS
{
    public partial class _Default : System.Web.UI.Page
    {
        Dictionary<string, List<string>> dlsPers = null;
        string sSalnum = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] != null)
            {
                Dictionary<string, List<string>> dlsLogin = Proces.DlsSqlSel("salnum,kenteken from personeel where salnum = '" + Session["login"].ToString() + "'", DateTime.Now);
                if (Session["login"] == null || dlsLogin[Session["login"].ToString().Trim()][0] != Session["pass"].ToString().Trim())
                {
                    Response.Redirect("http://localhost:56937/Login.aspx");
                }
                else
                {
                    sSalnum = Session["login"].ToString().Trim();
                }
                vulVelden();
            }
            else
            {
                Response.Redirect("http://localhost:56937/Login.aspx");
            }
        }

        protected void vulVelden()
        {
            System.Environment.SetEnvironmentVariable("TC_MANDANT", "FIL");
            dlsPers = Proces.DlsSqlSel("salnum,badnum,naam,init,telef,adres,pstkod,plaats from personeel where salnum = '" + sSalnum + "'", DateTime.Now);

            if (!Page.IsPostBack)
            {
                salnum.Text = sSalnum;
                naam.Text = dlsPers[sSalnum][1];
                init.Text = dlsPers[sSalnum][2];
                telef.Text = dlsPers[sSalnum][3];
                adres.Text = dlsPers[sSalnum][4];
                postcode.Text = dlsPers[sSalnum][5];
                plaats.Text = dlsPers[sSalnum][6];
            }
        }
        protected void opslaan()
        {
            StreamWriter sw = new StreamWriter(SB.Algemeen.Parameter.SSbRoot + "log\\tmp.imp");
            sw.WriteLine("personeel;badnum=" + dlsPers[sSalnum][0] + ";naam;" + this.naam.Text);
            sw.WriteLine("personeel;badnum=" + dlsPers[sSalnum][0] + ";init;" + this.init.Text);
            sw.WriteLine("personeel;badnum=" + dlsPers[sSalnum][0] + ";telef;" + this.telef.Text);
            sw.WriteLine("personeel;badnum=" + dlsPers[sSalnum][0] + ";adres;" + this.adres.Text);
            sw.WriteLine("personeel;badnum=" + dlsPers[sSalnum][0] + ";pstkod;" + this.postcode.Text);
            sw.WriteLine("personeel;badnum=" + dlsPers[sSalnum][0] + ";plaats;" + this.plaats.Text);

            sw.Close();
            Proces p = new Proces("dbimport " + SB.Algemeen.Parameter.SSbRoot + "log\\tmp.imp -e" + DateTime.Now.ToString("yyyyMMdd") + " -d;");
        }
        protected void submit(object sender, EventArgs e)
        {
            opslaan();
        }
        


    }
}