﻿using System;
using System.Collections.Generic;
using SB.Algemeen;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
namespace REB.ESS
{
    public partial class PeriodeTotaalOverzicht : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (IsPostBack)
            {
                string sPath = SB.Algemeen.Parameter.SSbRoot + "log\\select" + DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + ".wrk";
                StreamWriter sw = new StreamWriter(sPath);
                if (lbSelectie.SelectedItem != null)
                {
                    foreach (ListItem li in lbSelectie.Items)
                    {
                        if (li.Selected)
                        {
                            sw.Write(Proces.SLeesFile(Proces.FiSqlsel("badnum from personeel where " + ddlSelectie.SelectedItem.Text + " " + ddlSelopp.SelectedItem.Text + " '" + li.Value + "'").FullName));
                        }
                    }
                }
                else
                {
                    sw.Write(Proces.SLeesFile(Proces.FiSqlsel("badnum from personeel where " + ddlSelectie.SelectedItem.Text + " " + ddlSelopp.SelectedItem.Text + " '" + txtSel.Text + "'").FullName));
                }
                sw.Close();
                //FileInfo fiSelectie = Proces.FiSqlsel("badnum from personeel where " + ddlSelectie.SelectedItem.Text + " " + ddlSelopp.SelectedItem.Text + " '" + txtSelectie.Text + "'");
                FileInfo fi = Proces.FiPeriodeTotaalOverzicht(cdStartDatum.SelectedDate.ToString("yyMMdd"), cdEindDatum.SelectedDate.ToString("yyMMdd"), "1", ddlOverzichten.SelectedValue.ToString(), sPath, "1", "0");
                FileInfo fi2 = Proces.FiCsvrap(fi.FullName);
                string sInhoud = Proces.SLeesFile(fi2.FullName);
                this.divResult.InnerHtml = "<table border='1' cellpadding='0' cellspacing='0'>";
                int i = 0;
                List<decimal> ldTotaal = new List<decimal>();
                foreach (string sLijn in Regex.Split(sInhoud, Environment.NewLine))
                {

                    this.divResult.InnerHtml += "<tr>";
                    int a = 0;
                    foreach (string sData in sLijn.Split(';'))
                    {
                        
                        this.divResult.InnerHtml += "<td>";
                        this.divResult.InnerHtml += sData.Trim();
                        this.divResult.InnerHtml += "</td>";
                        try
                        {
                            ldTotaal[a] += Convert.ToDecimal(sData.Trim());
                        }
                        catch (ArgumentOutOfRangeException ex)
                        {
                            ldTotaal.Add(0m);
                        }
                        catch (Exception ex)
                        {
                            ldTotaal[a] = 0m;
                        }
                        a++;
                    }
                    this.divResult.InnerHtml += "</tr>";
                    i++;
                }
                this.divResult.InnerHtml += "<tr>";
                foreach (decimal d in ldTotaal)
                {
                    this.divResult.InnerHtml += "<td>" + d.ToString() + "</td>";
                }
                this.divResult.InnerHtml += "</tr>";
                this.divResult.InnerHtml += "</table>";
            }
        }

        protected void ddlOverzichten_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Dictionary<string, List<string>> dlsOverzichten = Proces.DlsSqlSel("recnr, omschr from pdeovdef");
                laadtDictionary(dlsOverzichten, ddlOverzichten.Items);
            }
        }

        protected void ddlSelectie_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Dictionary<string, List<string>> dlsSel = Proces.DlsSqlSel("volgnr, dbname from fields");
                foreach (KeyValuePair<string, List<string>> kvp in dlsSel)
                {
                    ddlSelectie.Items.Add(new ListItem(kvp.Value[0], kvp.Key));
                }
                ddlSelectie.SelectedIndex = 0;
            }
            
            ddlSelectie_SelectedIndexChanged(null, null);
        }

        protected void ddlSelopp_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Dictionary<string, List<string>> dlsSel = Proces.DlsSqlSel("volgnr, omschr from selopp");
                foreach (KeyValuePair<string, List<string>> kvp in dlsSel)
                {
                    ddlSelopp.Items.Add(new ListItem(kvp.Value[0], kvp.Key));
                }
            }
        }

        protected void ddlSelectie_SelectedIndexChanged(object sender, EventArgs e)
        {
            Dictionary<string, List<string>> dss = new Dictionary<string, List<string>>();
            switch (ddlSelectie.SelectedItem.Text.Trim())
            {
                case "badnum":
                    dss = Proces.DlsSqlSel("badnum, naam from personeel");
                    laadtDictionary(dss, lbSelectie.Items);
                    lbSelectie.Visible=true;
                    txtSel.Visible=false;
                    break;
                case "afdkod":
                    dss = Proces.DlsSqlSel("afdkod, afdoms from afdeling");
                    laadtDictionary(dss, lbSelectie.Items);
                    lbSelectie.Visible = true;
                    txtSel.Visible=false;
                    break;
                default:
                    lbSelectie.Visible = false;
                    txtSel.Visible=true;
                    break;
            }
        }

        

        protected void laadtDictionary(Dictionary<string, List<string>> dss,ListItemCollection lic)
        {
            //if (!IsPostBack)
            {
                lic.Clear();
                foreach (KeyValuePair<string, List<string>> kvp in dss)
                {
                    lic.Add(new ListItem(kvp.Key + " " + kvp.Value[0], kvp.Key));
                }
            }
        }
    }

}